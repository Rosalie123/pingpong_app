require 'rails_helper'

  RSpec.describe "PasswordResets", type: :feature do

    scenario "emails valid user when requesting password reset", js: true do
      user = create(:user)
      visit login_path
      click_on 'forgot password?'
      expect(current_path).to eq(new_password_reset_path)
      fill_in 'password_reset[email]', with: user.email
      click_on 'Submit'
      expect(current_path).to eq(root_path)
      expect(page).to have_content('email has been sent')
      expect(last_email.to).to include(user.email)
    end

    scenario "won't email if user email not found", js: true do
      visit login_path
      click_on 'forgot password?'
      expect(current_path).to eq(new_password_reset_path)
      fill_in 'password_reset[email]', with: 'nothing@example.com'
      click_on 'Submit'
      expect(page).to have_content("Email address doesn't exist")
      expect(last_email).to be_nil
    end

    scenario "won't email if user hasn't been activated", js: true do
      user = create(:non_activated_user)
      visit login_path
      click_on 'forgot password?'
      expect(current_path).to eq(new_password_reset_path)
      fill_in 'password_reset[email]', with: user.email
      click_on 'Submit'
      expect(page).to have_content("activate your account")
      expect(last_email).to be_nil
    end
  end
