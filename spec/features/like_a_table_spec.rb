require 'rails_helper'

RSpec.feature 'LikeATable', type: :feature do
  background do
    login
  end

  scenario 'will make heart go red when like', js: true do
    @table = create(:table)
    visit table_path(@table)
    find('.heart-hollow').click
    expect(page).to have_css('div.heart-red')
    visit user_path(@user)
    expect(page).to have_content("#{@table.address}")
  end

end
