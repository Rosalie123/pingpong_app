require 'rails_helper'

RSpec.describe "ActivateAccount", type: :feature do

  scenario "should send user activation email upon sign up", js: true do
    visit root_path
    click_on '#bat_agent'
    fill_in 'user[name]', with: 'example'
    fill_in 'user[email]', with: 'example@example.com'
    fill_in 'user[password]', with: 'password'
    fill_in 'user[password_confirmation]', with: 'password'
    expect(page).to have_content('confirm your email address')
    expect(last_email.to).to include(user.email)
  end
  
end
