require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before(:example) do
    @user = create(:user, email: 'yaoie@example.com')
  end

  it 'will log in user' do
    visit root_path
    fill_in 'session[email]', with: "yaoie@example.com"
    fill_in 'session[password]', with: 'password'
    click_button 'Log in'
    expect(page).to have_content "My profile"
  end

  it 'should redirect to log in' do
    visit user_path(create(:user))
    expect(page).to have_content 'Please log in!'
  end
end
