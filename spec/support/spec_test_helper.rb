module SpecTestHelper
  def log_user_in(user)
    post login_path, params: {session: {email: user.email, password: 'password'}}
  end

  def login
    @user = create(:user, email: 'user@example.com')
    visit login_path
    fill_in 'session[email]', with: 'user@example.com'
    fill_in 'session[password]', with: 'password'
    click_button 'Log in'
  end

end

module MailerHelper
  def last_email
    ActionMailer::Base.deliveries.last
  end

  def reset_email
    ActionMailer::Base.deliveries = []
  end
end
