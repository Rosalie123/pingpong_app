FactoryGirl.define do

  factory :user, aliases: [:proposer, :accepter, :bidder, :sender, :receiver, :author] do
    sequence(:name)       { |n| "Yaoie#{n}" }
    sequence(:email)      { |n| "#{name}@example.com"}
    admin                 false
    activated             true
    password              'password'
  end

  factory :non_activated_user, parent: :user do
    activated           false
  end



  factory :table do
    user
    sequence(:latitude) {|n| 42.2 + "#{n}".to_f}
    longitude           45.2
    table_rating        10.0
    surrounding_rating  10.0
    availability_rating 10.0
    ground_rating       10.0
    table_number         "1"
    parking             true
    toilet              true
    picnic              true
    playground          true
    sequence(:address)  {|n| "11 rue Victor#{n}, Paris, France"}
  end

  factory :match do
    start_at Time.now + 30.minutes
    accepter_id nil
    confirmed false
    table
    proposer
  end

  factory :match_bid, aliases: [:bid] do
    match
    bidder
    after(:create) do |u|
      create(:conversation, bid: u, match: u.match, sender: u.bidder,
              receiver: u.match.proposer)
    end
  end

  factory :accepted_bid, parent: :match_bid do
    accepted      true
    valid_bid     false
    # after(:build) do |u|
    #   create(:conversation, bid: u, match: u.match, sender: u.bidder,
    #           receiver: u.match.proposer, status: 'Accepted')
    # end
  end

  factory :conversation do
    sender
    receiver
    bid
    match
    subject 'drop'
    status 'pending'
  end

  # factory :message do
  #   author
  #   conversation
  # end
end
