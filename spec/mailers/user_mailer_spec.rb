require "rails_helper"

RSpec.describe UserMailer, :type => :mailer do
  describe "password reset" do
    let(:user) { create(:user) }
    let(:mail) { user.create_password_reset_digest
                 user.send_password_reset_email }

    it "send user password reset url" do
      expect(mail.subject).to eq("BATMAP: Password Reset")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq(['noreply@BATMAP.com'])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match(edit_password_reset_path(user.reset_token))
    end
  end

  describe "activate account" do
    let(:user) { create(:non_activated_user, activation_token: 'anything') }
    let(:mail) { user.send_activation_email }
    it "send user account activation url" do
      expect(mail.subject).to eq("BATMAP: One more step to become a BAT agent!")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq(['noreply@BATMAP.com'])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match(edit_user_activation_path(user.activation_token))
    end
  end

end
