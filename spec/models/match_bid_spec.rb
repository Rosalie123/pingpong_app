require 'rails_helper'

RSpec.describe MatchBid, type: :model do
  it "has a valid factory" do
    expect(build(:match_bid)).to be_valid
  end

  let(:existing_bid) { create(:match_bid) }
  let(:existing_match) {build(:match)}

  describe "Validations" do
    let(:bid) { build(:match_bid) }
    context 'upon creation' do
    # basic validations
      it 'it is not valid if same bidder has already bidded on the match' do
        bid.bidder_id = existing_bid.bidder_id
        bid.match_id = existing_bid.match_id
        expect(bid).to_not be_valid
      end

      it "is not valid if already accepted" do
        bid.accepted = true
        expect(bid).to_not be_valid
      end

      it "is not valid if it is not a open bid" do
        bid.valid_bid = false
        expect(bid).to_not be_valid
      end

    # complex validations
      it 'is not valid if the match has passed' do
        bid.match.start_at = Time.now
        expect(bid).to_not be_valid
      end

      it 'is not valid if bidder proposed a match at the same slot' do
        existing_match.proposer_id = bid.bidder_id
        existing_match.save
        expect(bid).to_not be_valid
      end

      it 'is not valid if bidder was accepted by a match at the same slot' do
        existing_match.save
        existing_match.update_columns(accepter_id: bid.bidder_id)
        expect(bid).to_not be_valid
      end

      it 'is not valid if match already confirmed' do
        bid.match.confirmed = true
        expect(bid).to_not be_valid
      end
    end

    context 'upon update to accept' do
      it 'is not valid if bid is no longer open' do
        existing_bid.valid_bid = false
        existing_bid.accepted = true
        expect(existing_bid).to_not be_valid
      end
      #
      # it 'is not valid if other bids have already been accepted' do
      #   existing_bid2 = create(:match_bid, match_id: existing_bid.match_id,
      #                                      bidder_id: create(:bidder).id)
      #   existing_bid.accepted = true
      #   existing_bid.save
      #   existing_bid2.accepted = true
      #   expect(existing_bid2).to_not be_valid
      # end
    end
  end

  describe 'callbacks' do
    let(:accepted_bid) { build(:accepted_bid)}
    let(:other_bid) { build(:match_bid) }

    describe 'after_update on acceptance' do
      context "callbacks run propoerly" do

        before(:example) do
          existing_bid.accepted = true
        end

        it 'should run disable bidder' do
          expect(existing_bid).to receive(:disable_bidder_for_same_slot)
          existing_bid.run_callbacks(:update)
        end

        it 'should not run enable bidder' do
          expect(accepted_bid).not_to receive(:enable_bidder_for_same_slot)
        end

        it 'should run disable other bids' do
          expect(existing_bid).to receive(:disable_other_bids_for_this_match)
          existing_bid.run_callbacks(:update)
        end

        it 'should not run enable other bids' do
          expect(accepted_bid).not_to receive(:enable_other_bids_for_this_match)
        end

        it 'should run update accepter' do
          expect(existing_bid).to receive(:accept_bidder_as_accepter)
          existing_bid.run_callbacks(:update)
        end

        it 'should not run delete accepter' do
          expect(accepted_bid).not_to receive(:delete_accepter_from_match)
        end

        it 'should run send notice' do
          expect(existing_bid).to receive(:send_notice_to_bidders)
          existing_bid.run_callbacks(:update)
        end
      end

      context "callback functions run properly" do
        it "should disable bidder's other bids for same slot" do
          bid = create(:match_bid, bidder_id: existing_bid.bidder_id)
          existing_bid.accepted = true
          expect {existing_bid.save}.to change { bid.reload.valid_bid}.from(true).to(false)
        end

        it "should disable match's other bids" do
          bid = create(:match_bid, match_id: existing_bid.match_id)
          existing_bid.accepted = true
          expect { existing_bid.save }.to change { bid.reload.valid_bid }.from(true).to(false)
        end

        it "should set other bidder's conversation status to refused" do
          bid = create(:match_bid, match_id: existing_bid.match_id)
          existing_bid.accepted = true
          expect{ existing_bid.save }.to change{ bid.conversation.reload.status }.from("pending").to("Refused")
        end

        it "should send notice to bidder's conversation" do
          existing_bid.accepted = true
          expect{ existing_bid.save }.to change{ existing_bid.conversation.messages.count }.by(1)
          expect(existing_bid.conversation.messages.last.content).to eq("#{existing_bid.conversation.receiver.name} accepted the bid")
        end

        it "should send notice to other bidder's conversation" do
          bid = create(:match_bid, match_id: existing_bid.match_id)
          existing_bid.accepted = true
          expect{ existing_bid.save }.to change{ bid.conversation.messages.count }.by(1)
          expect(bid.conversation.messages.last.content).to include("#{bid.conversation.receiver.name} accepted someone else")
        end

        it "should update match status" do
          existing_bid.accepted = true
          expect { existing_bid.save }.to change { existing_bid.match.reload.confirmed }.from(false).to(true)
        end

        it "should set accepted bidder's conversation status to accepted" do
          existing_bid.accepted = true
          existing_bid.save
          expect(existing_bid.conversation.reload.status).to eq("Accepted")
        end

        it "should update match accepter" do
          existing_bid.accepted = true
          expect { existing_bid.save}.to change {existing_bid.match.reload.accepter}.from(nil).to(existing_bid.bidder)
        end
      end
    end

    describe 'after_update on cancelation' do

      context "callbacks run propoerly" do
        before(:example) do
          accepted_bid.save(validate: false)
          accepted_bid.accepted = false
        end

        it 'should run enable bidder' do
          expect(accepted_bid).to receive(:enable_bidder_for_same_slot)
          accepted_bid.run_callbacks(:update)
        end

        it 'should not run disable bidder' do
          expect(existing_bid).not_to receive(:disable_bidder_for_same_slot)
        end

        it 'should run enable other bids' do
          expect(accepted_bid).to receive(:enable_other_bids_for_this_match)
          accepted_bid.run_callbacks(:update)
        end

        it 'should not run disable other bids' do
          expect(existing_bid).not_to receive(:disable_other_bids_for_this_match)
        end

        it 'should run delete accepter' do
          expect(accepted_bid).to receive(:delete_accepter_from_match)
          accepted_bid.run_callbacks(:update)
        end

        it 'should not run update accepter' do
          expect(existing_bid).not_to receive(:accept_bidder_as_accepter)
        end

        it 'should run send notice' do
          expect(accepted_bid).to receive(:send_notice_to_bidders)
          accepted_bid.run_callbacks(:update)
        end
      end

      context "callback functions run properly" do
        context "concerns other bids" do
          before(:example) do
            other_bid.valid_bid = false
            other_bid.save(validate: false)
          end

          it "should enable bidder's other bids for same slot" do
            accepted_bid.bidder_id = other_bid.bidder_id
            accepted_bid.save(validate: false)
            accepted_bid.accepted = false
            expect{ accepted_bid.save }.to change {other_bid.reload.valid_bid}.from(false).to(true)
          end

          it "should enable match's other bids" do
            accepted_bid.match_id = other_bid.match_id
            accepted_bid.save(validate: false)
            accepted_bid.accepted = false
            expect{ accepted_bid.save }.to change {other_bid.reload.valid_bid}.from(false).to(true)
          end

          context "modify conversations" do
            let!(:convo) {create(:conversation, sender_id: other_bid.bidder_id,
                                  receiver_id: other_bid.match.proposer_id,
                                  match_id: other_bid.match_id,
                                  bid_id: other_bid.id,
                                  status: 'Refused')}
            before(:example) do
              accepted_bid.match_id = other_bid.match_id
              accepted_bid.save(validate: false)
              accepted_bid.accepted = false
            end

            it "should reset other bids' conversations' status to pending" do
              expect{ accepted_bid.save }.to change {convo.reload.status}.from("Refused").to("Bid Pending")
            end

            it "should send cancel notice to other bidders" do
              expect{ accepted_bid.save }.to change { convo.messages.count }.by(1)
              expect(convo.messages.last.content).to include("#{convo.receiver.name}", "valid again")
            end
          end
        end

        context 'concerns canceled bid' do
          before(:example) do
            accepted_bid.save(validate: false)
          end
          let!(:convo) {create(:conversation, sender_id: accepted_bid.bidder_id,
                                receiver_id: accepted_bid.match.proposer_id,
                                match_id: accepted_bid.match_id,
                                bid_id: accepted_bid.id,
                                status: 'Accepted')}

          it "should reset accepted bid's conversation's status to pending" do
            accepted_bid.accepted = false
            expect{ accepted_bid.save }.to change {convo.reload.status}.from("Accepted").to("Bid Pending")
          end

          it "should send cancel notice to accepted bidder" do
            accepted_bid.accepted = false
            expect{ accepted_bid.save }.to change { convo.messages.count }.by(1)
            expect(convo.messages.last.content).to eq("#{convo.receiver.name} canceled the acceptance")
          end

        end

        context "concerns bid's match" do
          before(:example) do
            accepted_bid.save(validate: false)
            accepted_bid.match.accepter_id = accepted_bid.id
            accepted_bid.match.confirmed = true
            accepted_bid.match.save(validate: false)
            accepted_bid.accepted = false
          end

          it "should remove accepter from match" do
            expect { accepted_bid.save }.to change{ accepted_bid.match.reload.accepter_id }.from(accepted_bid.id).to(nil)
          end

          it "should reset match to unconfirmed" do
            expect { accepted_bid.save }.to change{ accepted_bid.match.reload.confirmed }.from(true).to(false)
          end
        end
      end

    end

    describe 'before_destroy' do
      describe "delete accepted bid" do
        context "callback runs properly" do
          before(:example) do
            accepted_bid.save(validate: false)
          end
          let!(:convo){ create(:conversation, sender_id: accepted_bid.bidder_id,
                                receiver_id: accepted_bid.match.proposer_id,
                                match_id: accepted_bid.match_id,
                                bid_id: accepted_bid.id)}
          it 'should run delete accepter' do
            accepted_bid.destroy
            expect(accepted_bid).to receive(:delete_accepter_from_match)
            accepted_bid.run_callbacks(:destroy)
          end

          it 'should run enable other bids ' do
            accepted_bid.destroy
            expect(accepted_bid).to receive(:enable_other_bids_for_this_match)
            accepted_bid.run_callbacks(:destroy)
          end

          it 'should run enable other bids for bidder' do
            accepted_bid.destroy
            expect(accepted_bid).to receive(:enable_bidder_for_same_slot)
            accepted_bid.run_callbacks(:destroy)
          end

          it 'should run send notice to bidder' do
            accepted_bid.destroy
            expect(accepted_bid).to receive(:send_notice_to_bidders_upon_bid_destroy)
            accepted_bid.run_callbacks(:destroy)
          end

          it 'should run send notice to proposer' do
            accepted_bid.destroy
            expect(accepted_bid).to receive(:send_notice_to_proposer)
            accepted_bid.run_callbacks(:commit)
          end
        end

        context "callback functions run properly" do
          context 'concerns other bid' do
            before(:example) do
              other_bid.valid_bid = false
              other_bid.save(validate: false)
            end

            it 'should enable other bids' do
              accepted_bid.match_id = other_bid.match_id
              accepted_bid.save(validate: false)
              convo = create(:conversation, sender_id: accepted_bid.bidder_id,
                                    receiver_id: accepted_bid.match.proposer_id,
                                    match_id: accepted_bid.match_id,
                                    bid_id: accepted_bid.id)
              expect{ accepted_bid.destroy }.to change {other_bid.reload.valid_bid}.from(false).to(true)
            end

            it "should enable bidder's bids" do
              accepted_bid.bidder_id = other_bid.bidder_id
              accepted_bid.save(validate: false)
              convo = create(:conversation, sender_id: accepted_bid.bidder_id,
                                    receiver_id: accepted_bid.match.proposer_id,
                                    match_id: accepted_bid.match_id,
                                    bid_id: accepted_bid.id)
              expect{ accepted_bid.destroy }.to change {other_bid.reload.valid_bid}.from(false).to(true)
            end

            context "modify conversations" do
              let!(:convo1) {create(:conversation, sender_id: other_bid.bidder_id,
                                    receiver_id: other_bid.match.proposer_id,
                                    match_id: other_bid.match_id,
                                    bid_id: other_bid.id,
                                    status: 'Refused')}
              before(:example) do
                accepted_bid.match_id = other_bid.match_id
                accepted_bid.save(validate: false)
              end
              let!(:convo) {create(:conversation, sender_id: accepted_bid.bidder_id,
                                    receiver_id: accepted_bid.match.proposer_id,
                                    match_id: accepted_bid.match_id,
                                    bid_id: accepted_bid.id,
                                    status: 'Accepted')}
              it "should reset other bids' conversations' status to pending" do
                expect{ accepted_bid.destroy }.to change {convo1.reload.status}.from("Refused").to("Bid Pending")
              end

              it "should send cancel notice to other bidders" do
                expect{ accepted_bid.destroy }.to change { convo1.messages.count }.by(1)
                expect(convo1.messages.last.content).to include("accepter canceled", "valid again")
              end
            end
          end

          context 'concerns deleted bid' do
            it "should send notice to proposer" do
              accepted_bid.save(validate: false)
              convo = create(:conversation, sender_id: accepted_bid.bidder_id,
                                    receiver_id: accepted_bid.match.proposer_id,
                                    match_id: accepted_bid.match_id,
                                    bid_id: accepted_bid.id)
              expect{ accepted_bid.destroy }.to change { convo.messages.count }.by(1)
              expect(convo.messages.last.content).to eq("#{accepted_bid.bidder.name} dropped the bid")
              expect(convo.reload.status).to eq("Bid dropped")
            end
          end

          context 'concerns match' do
            it 'should delete accepter in match' do
              accepted_bid.save(validate: false)
              convo = create(:conversation, sender_id: accepted_bid.bidder_id,
                                    receiver_id: accepted_bid.match.proposer_id,
                                    match_id: accepted_bid.match_id,
                                    bid_id: accepted_bid.id)
              accepted_bid.match.accepter_id = accepted_bid.bidder_id
              accepted_bid.match.confirmed = true
              accepted_bid.match.save(validate: false)
              expect{ accepted_bid.destroy }.to change {accepted_bid.match.reload.accepter}.from(accepted_bid.bidder).to(nil)
              expect(accepted_bid.match.confirmed).to be false
            end
          end
        end
      end

      describe "delete unaccepted bid" do
        context "callbacks run properly" do
          before(:example) { existing_bid.destroy }
          it "should run notice to proposer" do
            expect(existing_bid).to receive(:send_notice_to_proposer)
            existing_bid.run_callbacks(:commit)
          end

          it "should not run notice to bidders" do
            expect(existing_bid).not_to receive(:send_notice_to_proposer)
          end
        end
        context "callback functions run properly" do
          it "should send notice to proposer" do
            expect{existing_bid.destroy}.to change{existing_bid.conversation.messages.count}.by(1)
            expect(existing_bid.conversation.messages.last.content).to eq("#{existing_bid.bidder.name} dropped the bid")
          end
        end
      end
    end
  end

end
