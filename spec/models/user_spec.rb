require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'asks for a password reset' do
    let(:user) {create(:user)}

    it 'creates a unique password reset token each time' do
      user.create_password_reset_digest
      last_token = user.reset_token
      user.create_password_reset_digest
      expect(user.reset_token).to_not eq(last_token)
    end

    it 'saves the time the password reset was sent' do
      user.create_password_reset_digest
      expect(user.reset_sent_at).to be_present
    end

    it 'delivers email to user' do
      user.create_password_reset_digest
      user.send_password_reset_email
      expect(last_email.to).to include(user.email)
    end

  end

  describe 'sign up' do
    it 'is valid with valid attributes' do
      user = build(:user)
      expect(user).to be_valid
    end
  end

  describe 'send activation email' do
    let(:user) { create(:non_activated_user) }
    it 'creates unique user activation token upon creation' do
      expect(user.activation_token).to_not be_nil
      user2 = create(:non_activated_user)
      expect(user2.activation_token).to_not eq(user.activation_token)
    end

    it 'delivers activation email to user' do
      user.send_activation_email
      expect(last_email.to).to include(user.email)
    end
  end

end
