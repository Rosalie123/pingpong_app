require 'rails_helper'

RSpec.describe Match, type: :model do

  let(:match) { build(:match) }
  let(:existing_match) { create(:match) }

  context 'upon creation' do

    it 'is valid with valid attributes' do
      expect(match).to be_valid
    end

    it 'is not valid without valid start time' do
      match.start_at = Time.now - 1.minute
      expect(match).to_not be_valid
    end

    it 'is not valid if accepter exists upon creation' do
      match.accepter_id = 1
      expect(match).to_not be_valid
    end

    it 'is not valid if match is confirmed upon creation' do
      match.confirmed = true
      expect(match).to_not be_valid
    end

    it 'is not valid if proposer has proposed another match at same time' do
      match.proposer_id = existing_match.proposer.id
      match.start_at = Time.now + 15.minutes
      expect(match).to_not be_valid
    end

    it 'is valid if proposer has proposed another match at different time' do
      match.proposer_id = existing_match.proposer.id
      match.start_at = Time.now + 60.minutes
      expect(match).to be_valid
    end

    it 'is not valid if table is not available at current time slot' do
      match.table_id = existing_match.table.id
      match.start_at = Time.now + 45.minutes
      expect(match).to_not be_valid
    end

    it 'is valid if match starts on same table after end time of another match' do
      match.table_id = existing_match.table.id
      match.start_at = Time.now + 60.minutes
      expect(match).to be_valid
    end

    it 'is not valid if proposer has bids at the same time slot as proposed match' do
      bid = create(:match_bid)
      existing_match = bid.match
      match.proposer_id = bid.bidder.id,
      match.start_at = Time.now + 20.minutes
      expect(match).to_not be_valid
    end

    it 'is not valid if proposer has accepted another match at same time slot' do
      accepter = create(:accepter)
      existing_match.update_column(:accepter_id, accepter.id)
      match.proposer_id = existing_match.accepter_id,
      match.start_at = Time.now + 32.minutes
      expect(match).to_not be_valid
    end
  end

  context 'upon update' do
    it 'can not be changed if after current time' do
      existing_match.update_attributes(start_at: Time.now)
      expect(existing_match).to_not be_valid
    end

    it 'is not valid when accepter is proposer' do
      existing_match.update_column(:accepter_id, existing_match.proposer_id)
      expect(existing_match).to_not be_valid
    end

    it 'is not valid when accepter did not bid for match' do
      accepter = create(:accepter)
      existing_match.update_attributes(accepter_id: accepter.id)
      expect(existing_match).to_not be_valid
    end

    it 'is not valid to change time slot when bids already exist' do
      bid = create(:match_bid)
      match = bid.match
      match.update_attributes(start_at: Time.now + 50.minutes)
      expect(match).to_not be_valid
    end
  end

  context 'before_validation' do

    it 'should run the proper callbacks' do
      expect(match).to receive(:load_end_time)
      match.run_callbacks(:validation)
    end

    it 'should load match end time to be 30 minutes after start time' do
      expect{ match.save }.to change{match.end_at}.from(nil).to(match.start_at + 30.minutes)
    end
  end

  context 'before_destroy' do

    it 'should run the proper callbacks' do
      expect(match).to receive(:send_notice_to_bidders)
      match.run_callbacks(:destroy)
    end

    it 'should send notice to bidders when match destroyed' do
      bid = create(:match_bid)
      conversation = create(:conversation, sender_id: bid.bidder_id,
                            receiver_id: bid.match.proposer_id, match_id: bid.match.id)
      expect { bid.match.destroy }.to change { conversation.messages.count}.by(1)
    end
  end

end
