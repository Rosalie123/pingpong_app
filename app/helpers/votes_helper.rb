module VotesHelper
  def already_voted?(comment)
    enable_vote = false
    comment.voters.each do |voter|
      if voter == current_user
        enable_vote = true
        break
      end
     end
    return enable_vote
  end

  def vote(comment)
    vote = comment.votes.where(voter_id: current_user.id).first.id
  end
end
