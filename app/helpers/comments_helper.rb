module CommentsHelper

  def best(comments)
    count = 0
    best = comments.first
    comments.each do |comment|
      if comment.votes.count > count && comment.parent.nil?
        count = comment.votes.count
        best = comment
      end
    end
    best
  end

end
