module ApplicationHelper

  def full_title (page_title = '')
    base_title = "BATMAP"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def already_bidded?(match)
    enable_bid = false
    match.bidders.each do |bidder|
      if bidder.name == current_user.name
        # @bid = MatchBid.find_by(bidder_id: bidder.id)
        enable_bid = true
        break
      end
     end
    return enable_bid
  end

  def get_bid(match, user)
    @bid = MatchBid.where(match_id: match.id).where(bidder_id: user.id).first
  end

  def bids_count(match)
    match.match_bids.count
  end

# notifications

  def notifications(user)
     if unread_count(user) > 5
       unread(user)
     else
       unread(user) + user.received_notifications.where(read: true)
                        .order('created_at DESC').limit(5 - unread_count(user))
     end
  end

  def unseen(user)
    user.received_notifications.where(seen: false).order('created_at DESC')
  end

  def has_unseen?(user)
    user.received_notifications.where(seen: false).any?
  end

  def unseen_count(user)
    unseen(user).count
  end

  def unread_count(user)
    unread(user).count
  end

  def unread(user)
    user.received_notifications.where(read: false).order('created_at DESC')
  end


# conversation_presenter

  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end
end
