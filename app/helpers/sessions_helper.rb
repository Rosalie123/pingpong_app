module SessionsHelper

  def log_in(user)
    session[:user_id] = user.id
  end

  def logged_in?
    !current_user.nil?
  end

  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
       user = User.find_by(id: user_id)
       if user && user.authenticated?(:remember, cookies[:remember_token])
         @current_user = user
       end
    end
  end

  def admin_user?
    current_user.admin if current_user
  end


# log out user
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
    flash[:danger] = "Logged out"
  end

  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies[:remember_token] = { value: user.remember_token,
                                 expires: 3.years.from_now.utc}
    cookies.signed[:user_id] = { value: user.id,
                                 expires: 3.years.from_now.utc}
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

end
