class Rating < ApplicationRecord

  belongs_to :user
  belongs_to :table

  before_save :get_overal

  validates :availability_rating, presence: true
  validates :table_rating, presence: true
  validates :surrounding_rating, presence: true
  validates :ground_rating, presence: true

  after_create_commit { RatingBroadcastJob.perform_later(self)}

  def get_overal
    self.overal_rating = (availability_rating + table_rating +
                          surrounding_rating + ground_rating)/4
  end

  def update_rating(table)
   self.update_attributes( table_rating:        table.table_rating,
                           availability_rating: table.availability_rating,
                           ground_rating:       table.ground_rating,
                           surrounding_rating:  table.surrounding_rating,
                           user_id:             table.user_id)
  end

end
