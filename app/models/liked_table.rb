class LikedTable < ApplicationRecord
  belongs_to :liker, class_name: 'User'
  belongs_to :table, class_name: 'Table'
  validates_uniqueness_of :table_id, scope: [:liker_id]
end
