class Conversation < ApplicationRecord

  include ShortDate

  attr_readonly :sender_id, :receiver_id, :match_id, :subject

  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :bid, class_name: 'MatchBid', optional: true
  belongs_to :match, optional: true

  has_many :messages, dependent: :destroy, inverse_of: :conversation
  has_many :notifications
  accepts_nested_attributes_for :messages, reject_if: lambda { |a| a[:content].blank? }




  validates :bid, uniqueness: true, if: :bid_exist?
  validate  :participant_difference

  scope :participating, -> (user){where(sender_id: user.id).or(
          Conversation.where(receiver_id: user.id)).order('updated_at DESC')}

  scope :bids, -> { where.not(bid_id: nil) }
  scope :my_matches, -> (user) { where(receiver_id: user.id).where.not(match_id: nil)}
  scope :my_bids, -> (user){ where(sender_id: user.id).where.not(match_id: nil)}
  scope :my_PM, -> { where(status: nil)}
  scope :unread, -> (user){where(receiver_id: user.id).where(read_receiver: false)
                        .or(where(sender_id: user.id).where(read_sender: false))}



  after_create_commit { ConversationBroadcastJob.perform_later(self) }

  def last_msg
    self.messages.last if self.messages.any?
  end

  def Conversation.filter(params, user)
    self.send("#{params}", user).participating(user) unless params.nil?
  end


  private

  def bid_exist?
    !self.bid_id.blank?
  end

  def get_bid
    @bid = MatchBid.find(bid_id)
  end

  def participant_difference
    errors.add(:sender, "can't message to yourself") if
    (sender_id == receiver_id)
  end

end
