class User < ApplicationRecord

  include ShortDate

  attr_accessor :activation_token, :remember_token, :reset_token

  before_create :create_active_digest
  before_save :downcase_email
  has_many :received_notifications, class_name: 'Notification',
                                    foreign_key: 'receiver_id'
  has_many :sent_notifications,class_name: 'Notification',
                               foreign_key: 'notifier_id'
  has_many :liked_tables, class_name: 'LikedTable', foreign_key: 'liker_id'
  has_many :wish_tables, through: :liked_tables, source: :table
  has_many :rated_tables, through: :ratings, source: :table
  has_many :comments
  has_many :votes, foreign_key: 'voter_id'
  has_many :tables
  has_many :ratings
  has_many :proposed_matches, class_name: "Match", foreign_key: "proposer_id",
                                                   dependent: :destroy

  has_many :accepted_matches, class_name: "Match", foreign_key: "accepter_id"


  has_many :bids, class_name: "MatchBid", foreign_key: "bidder_id",
                                                    dependent: :destroy

  has_many :bidded_matches, through: :bids, foreign_key: "bidder_id",
           source: :match

  has_many :sent_conversations, class_name: 'Conversation',
                                   foreign_key: 'sender_id',
                                   dependent: :destroy

  has_many :received_conversations, class_name: 'Conversation',
                                 foreign_key: 'receiver_id',
                                 dependent: :destroy
  has_many :messages, foreign_key: 'author_id', inverse_of: :author








  validates :name, presence: true, length: { maximum: 200 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence:true, length: { maximum: 255 },
                    format:  { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false}

  has_secure_password
  validates :password, presence: true, length: { minimum: 8}, allow_nil: true




  # Returns a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def send_activation_email
    UserMailer.user_activate(self).deliver_now
  end

  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end


  def activate
    update_columns(activated: true, activated_at: Time.zone.now,
                   activation_digest: nil)
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def create_password_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token),
                   reset_sent_at: Time.zone.now)
  end

  def password_reset_expired
    reset_sent_at < 2.hours.ago
  end

  def delete_reset_digest
    update_attribute(:reset_digest, nil)
  end

  def matches
    self.proposed_matches.where(confirmed: true).count +
    self.accepted_matches.where(confirmed: true).count
  end








  private

  # assign active_token and active_digest upon Signup

  def create_active_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

  def downcase_email
    self.email = email.downcase
  end






end
