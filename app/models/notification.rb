class Notification < ApplicationRecord
  belongs_to :conversation, optional: true
  belongs_to :bid, class_name: 'MatchBid', optional: true
  belongs_to :notifier, class_name: 'User', optional: true
  belongs_to :receiver, class_name: 'User'

  validates :content, presence: true
  validates :title, presence: true

end
