class Vote < ApplicationRecord
  belongs_to :comment, counter_cache: true
  belongs_to :voter, class_name: 'User'

  validates_uniqueness_of :comment_id, scope: [:voter_id]
end
