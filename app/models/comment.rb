class Comment < ApplicationRecord

  belongs_to :user
  belongs_to :table, optional: true
  has_many :replies, class_name: 'Comment', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Comment', optional: true
  has_many :votes, dependent: :destroy
  has_many :voters, through: :votes

  validates :content, presence: true
  validates :title, presence: true

  scope :best, -> (table){where(table_id: table.id).order('votes_count DESC').first}

end
