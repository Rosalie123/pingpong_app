class MatchBid < ApplicationRecord

  attr_readonly :bidder_id, :match_id, :valid_bid

#  Association:
  belongs_to :match
  belongs_to :bidder, class_name: "User"
  has_one    :conversation, foreign_key: 'bid_id'
  has_many   :notifications, foreign_key: 'bid_id'

# Validation:
  validates_uniqueness_of :match_id, scope: :bidder_id, on: :create
  validates :accepted, inclusion: {in: [false]}, on: :create
  validates :valid_bid, inclusion: {in: [true]}, on: :create
  validate  :match_passed

  #  upon create
  validate :bidder_available, on: :create
  validate :match_available, on: :create

  #  upon update
  validate :no_accepted_bid, on: :update, if: :to_accept?
  validate :bid_valid, on: :update, if: :to_accept?

# Callbacks:
  after_update :send_notice_to_bidders, on: :update

  # to_accept
  after_update :disable_bidder_for_same_slot, if: :to_accept?, on: :update
  after_update :disable_other_bids_for_this_match, if: :to_accept?, on: :update
  after_update  :accept_bidder_as_accepter, if: :to_accept?, on: :update

  # to_cancel
  after_update :enable_bidder_for_same_slot, if: :to_cancel?, on: :update
  after_update :enable_other_bids_for_this_match, if: :to_cancel?, on: :update
  after_update :delete_accepter_from_match, if: :to_cancel?, on: :update


  before_destroy :prevent_destroy

  # delete (if bid was accepted)
  after_destroy  :delete_accepter_from_match, if: :accepted?
  after_destroy  :enable_other_bids_for_this_match, if: :accepted?
  after_destroy  :enable_bidder_for_same_slot, if: :accepted?
  after_destroy  :send_notice_to_bidders_upon_bid_destroy, if: :accepted?

  # delete (if bid wasn't accepted)
  after_destroy_commit :send_notice_to_proposer





  private

  # upon creation:
  # validation:
  # check if bidder already had matches proposed/accepted for this time slot
  def bidder_available
    matches = Match.where(proposer_id: bidder_id).or(Match.where(
                          accepter_id: bidder_id)).where(
                          "start_at < ? AND end_at > ?",
                          match.end_at, match.start_at) unless self.match.nil?
      errors.add(:bidder, "You already have other matches confirmed
                for this time slot")  unless matches.blank?
  end

  # check if match is still open for bidding
  def match_available
    errors.add(:match_id, "This match is already confirmed") if
    ((!self.match.nil?) && (self.match.confirmed))
  end

  # check if match is passed
  def match_passed
    errors.add(:match, 'is already passed') if self.match.start_at <= Time.now
  end

  def prevent_destroy
    if self.match.start_at <= Time.now
      errors.add(:match, 'is already passed')
      throw :abort
    end
  end


  # upon update:

  # 1/ accept a bidder for a match (put accepted from false to true)
    def to_accept?
      (self.accepted_changed?) && (self.accepted == true)
    end
    # validation
    # a. check if same match has already been accepted
    def no_accepted_bid
      bids  = MatchBid.where(match_id: match_id).where(accepted: true)
      errors.add(:bidder, "You already accepted one bidder, you need to cancel
                 first before proceeding") unless bids.empty?
    end

    # b. check if bid about to accept is valid
    def bid_valid
      errors.add(:bid, "The bid is no longer valid as the bidder
                 has been accepted for another match") unless self.valid_bid?
    end

    # callbacks
    # a. invalidate bidder's other bids in same slot

    def disable_bidder_for_same_slot
      unless self.match.nil?
        MatchBid.joins(:match).where(
        "matches.start_at < ? AND matches.end_at > ? AND match_bids.bidder_id =? ",
        self.match.end_at, self.match.start_at, bidder_id).update_all(valid_bid: false)
      end
    end

    # b. invalidate other user's bit for this match

    def disable_other_bids_for_this_match
      MatchBid.where(match_id: match_id).update_all(valid_bid: false)
      Conversation.where(match_id: match_id)
      .where.not(bid_id: id).where.not(bid_id: nil).update_all(status: 'Refused')
    end

    # c. update accepter_id and confirmed to Match
    def accept_bidder_as_accepter
      Match.find_by(id: match_id).update_columns(accepter_id: bidder_id,
                                                 confirmed: true)
      Conversation.find_by(bid_id: id).update_columns(status: 'Accepted')
    end

  # 2/ cancel acceptance of a bidder (put accepted from true to false)
    def to_cancel?
        (self.accepted_changed?) && (self.accepted == false)
    end
    # validation (none)
    # callbacks

    # a. reactivate chosen user's other bids
    def enable_bidder_for_same_slot
      unless self.match.nil?
        MatchBid.joins(:match).where(
        "matches.start_at < ? AND matches.end_at > ? AND match_bids.bidder_id =? ",
        match.end_at, match.start_at, bidder_id).update_all(valid_bid: true)
      end
    end

    # b. reactivate bids for this match

    def enable_other_bids_for_this_match
      MatchBid.where(match_id: match_id).update_all(valid_bid: true)
      Conversation.where(match_id: match_id)
      .where.not(bid_id: nil).update_all(status: 'Bid Pending')
    end
    # c. remove bidder as accepter



  # 3/ cancel bid (when bid already accepted)
    def delete_accepter_from_match
      Match.find_by(id: match_id).update_columns(accepter_id: nil,
                                                  confirmed: false)
    end


  # upon distroy:
    def accepted?
      self.accepted == true
    end

    def not_accepted?
      self.accepted == false
    end

    def send_notice_to_proposer
      conversation = self.conversation
      conversation.update_columns(status: 'Bid dropped',
                                  bid_id: nil)
      conversation.messages.create(author_id: conversation.sender.id,
                                   notice: true,
                                   content: conversation.sender.name +
                                            ' dropped the bid')
    end

  def send_notice_to_bidders
      self.match.conversations.each do |conversation|
        unless conversation.bid_id.nil?
            content_title = conversation.receiver.name
          if (self.conversation == conversation) && (to_accept?)
            content = ' accepted the bid'
          elsif (self.conversation != conversation) && (to_accept?)
            content = ' accepted someone else, the bid is no longer valid'
          elsif (self.conversation == conversation) && (to_cancel?)
            content = ' canceled the acceptance'
          else
            content = ' canceled the acceptance, the bid is now valid again'
          end
            conversation.messages.create(author_id: conversation.receiver.id,
                                         notice: true,
                                         content: content_title + content)
        end
      end
  end

  def send_notice_to_bidders_upon_bid_destroy
      self.match.conversations.each do |conversation|
          if self.conversation != conversation && !conversation.bid_id.nil?
            conversation.messages.create(author_id: conversation.receiver.id,
                                         notice: true,
                                         content: "The match's accepter canceled the bid, the bid is now valid again")
          end
      end
  end

end
