class Table < ApplicationRecord

  acts_as_mappable  default_units: :kms,
                    lat_column_name: :latitude,
                    lng_column_name: :longitude

  before_save :get_address, on: :create
  before_save :get_overal


  has_many :comments, dependent: :destroy
  belongs_to :user
  has_many :pictures, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :matches, dependent: :delete_all
  has_many :likers, class_name: 'LikedTable', foreign_key: 'table_id'
  has_many :wishers, through: :likers, source: :liker

  validate  :avoid_duplicate, on: :create
  validates :longitude, presence: true
  validates :latitude, presence: true
  validates :availability_rating, presence: true
  validates :table_rating, presence: true
  validates :surrounding_rating, presence: true
  validates :ground_rating, presence: true
  validates :parking, inclusion: {in: [true, false]}
  validates :toilet, inclusion: {in: [true, false]}
  validates :playground, inclusion: {in: [true, false]}
  validates :picnic, inclusion: {in: [true, false]}
  validates :table_number, presence: true
            #  inclusion: { in: ["1", "2", "3", "4", "5 or more"]}

  after_create_commit { TableBroadcastJob.perform_later(self)}


  def update_rating(rating)
   count = ratings.count - 1
   self.update_attributes(table_rating:   ((rating.table_rating+
                                            self.table_rating*count)/(count+1)),

                     availability_rating: ((rating.availability_rating+
                                            self.availability_rating*count)/(count+1)),

                     ground_rating:       ((rating.ground_rating +
                                          self.ground_rating*count)/(count+1)),

                     surrounding_rating:  ((rating.surrounding_rating +
                                          self.surrounding_rating*count)/(count+1))
                    )
  end

  def my_rating(user)
    self.ratings.where(user_id: user.id).first
  end



  private

  def get_overal
    self.overal_rating = (availability_rating + table_rating +
                          surrounding_rating + ground_rating)/4
  end

  def get_address
    res=Geokit::Geocoders::GoogleGeocoder.reverse_geocode "#{self.latitude}, #{self.longitude}"
    errors.add(:address, "Could not save address") if !res.success
    self.address = res.full_address if res.success
  end

  def avoid_duplicate
    center = [latitude, longitude]
    tables = Table.within(0.1, units: :kms, origin: center)
    if tables.any?
      errors.add(:table, "is already in BATMAP, please double check
                          if it's not the same one")
    end
  end

end
