class Message < ApplicationRecord

  include ShortDate

  belongs_to :conversation, inverse_of: :messages
  belongs_to :author, class_name: 'User', inverse_of: :messages

  validates :content, presence: true
  before_validation :set_author



  after_create_commit { MessageBroadcastJob.perform_later(self, self.conversation) }
  after_create_commit { NotificationBroadcastJob.perform_later(self, self.conversation)}

  private
  def set_author
    self.author_id = self.conversation.sender_id if self.author_id.nil?
  end

end
