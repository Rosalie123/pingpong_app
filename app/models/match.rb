class Match < ApplicationRecord

  include ShortDate

  attr_readonly :proposer_id, :table_id


  belongs_to :accepter, class_name: "User", optional: true
  belongs_to :proposer, class_name: "User"
  belongs_to :table
  has_many   :match_bids, dependent: :delete_all
  has_many   :bidders, through: :match_bids, source: :bidder
  has_many   :conversations


  before_validation :load_end_time
  before_destroy    :prevent_destroy
  after_destroy     :send_notice_to_bidders


  # validates :start_at, presence: true
  validates :end_at, presence: true
  validates :accepter_id, inclusion: {in: [nil]}, on: :create
  validates :confirmed, inclusion: {in: [false]}, on: :create
  validate  :valid_start_time
  validate  :proposer_bids
  validate  :table_available
  validate  :proposer_available
  validate  :participant_difference, on: :update
  validate  :freeze_time_slot, on: :update
  validate  :accepter_is_a_bidder, on: :update

  scope :this_table, -> (table) {where(table_id: table.id)}

  scope :time_slot, -> (start_at, end_at) { where(start_at: start_at..end_at)}

  scope :confirmed, ->   { where(confirmed: true)}

  scope :proposed, ->    { where(confirmed: false)}

  scope :proposer_me, -> (user) { where(proposer_id: user.id)}

  scope :accepter_me, -> (user) { where(accepter_id: user.id)}

  scope :participant_me, -> (user) {where(accepter_id: user.id).or(where(proposer_id: user.id))}

  scope :proposer_not_me, -> (user) { where.not(proposer_id: user.id)}

  scope :accepter_not_me, -> (user) { where.not(accepter_id: user.id)}

  scope :i_bidded, -> (user) { joins(:match_bids).where(match_bids: { bidder_id: user.id})}




  private

  def valid_start_time
    errors.add(:you, "can't arrange/change match before current time") if
    (start_at < Time.now)
  end

  def participant_difference
    errors.add(:you, "can't accept your own match") if
    (accepter_id == proposer_id)
  end

  def table_available
    matches = Match.where(table_id: table_id).where("start_at < ? AND end_at > ?",
             end_at, start_at)
    unless matches.empty? or (matches.first == self && matches.length == 1)
      errors.add(:table, "not available for this time slot")
    end
  end

  def proposer_available
    matches = Match.where(proposer_id: proposer_id).or(Match.where(
              accepter_id: proposer_id)).where.not(table_id: table_id).where(
              "start_at < ? AND end_at > ?", end_at, start_at)
    unless matches.empty?
      errors.add(:you, "already have other matches proposed or accepted
                                                          for this time slot")
    end
  end

  def proposer_bids
      bids = MatchBid.joins(:match).where(
          "matches.start_at < ? AND matches.end_at > ? AND match_bids.bidder_id =? ",
          end_at, start_at, proposer_id.to_s)
      unless bids.empty?
        errors.add(:you, "have bidded on matches with the same time slot,
                cancel the bids first before you propose a match with the same
                 slot")
      end
  end


  def freeze_time_slot
    unless self.match_bids.empty?
      errors.add(:you, "can not change time slot as bids already exist") if
      (start_at_changed? || end_at_changed?)
    end
  end

  def load_end_time
    self.end_at = start_at + 30.minutes unless start_at.nil?
  end

  def accepter_is_a_bidder
    bid = MatchBid.where(match_id: id).where(valid_bid: true).where(
                                                      bidder_id: accepter_id)
    if ((bid.empty?) && (accepter_id != nil))
      errors.add(:accepter_id, "The accepter didn't bid for this match")
    end
  end

  def prevent_destroy
    if self.start_at <= Time.now
      errors.add(:match, 'is already passed')
      throw :abort
    end
  end

  def send_notice_to_bidders
    if !self.accepter_id.nil?
      conversations = self.conversations.where(sender_id: accepter_id)
    else
      conversations = self.conversations
    end
    conversations.each do |conversation|
      conversation.messages.create(author_id: conversation.receiver.id,
                                   notice: true,
                                   content: conversation.receiver.name +
                                                          ' canceled the match')
    end
  end

end
