module ShortDate
  extend ActiveSupport::Concern

  def render_date
    self.created_at.to_formatted_s(:short)
  end

  def render_update_date
    self.updated_at.to_formatted_s(:short)
  end

  def render_year
    self.created_at.to_formatted_s(:long_ordinal)
  end

  #
  # module ClassMethods
  #   def
  #   end
  # end

end
