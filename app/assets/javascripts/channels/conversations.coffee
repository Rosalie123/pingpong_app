App.conversations = App.cable.subscriptions.create "ConversationsChannel",

  convos: -> $('.conversations')

  connected: ->
    setTimeout =>
      @followInbox()
      @installPageChangeCallback()
    , 1000

  disconnected: ->

  followInbox: ->
    if @convos().length
      @perform 'follow'
    else
      @perform 'unfollow'


  received: (data) ->
    convo = $('#conversation_' + data['conversation_id'])
    if data['conversation_id']
      convo.addClass('unread').find('.last-message').text(data['message'])
      convo.find('.conversation-status').text(data['status'])
      convo.find('.conversation-date').text(data['date'])
      $('.conversations').prepend(convo);
    else
      @convos().prepend(data['convo'])


  installPageChangeCallback: ->
    unless @installedPageChangeCallback
      @installedPageChangeCallback = true
      $(document).on 'turbolinks:load', -> App.conversations.followInbox()
