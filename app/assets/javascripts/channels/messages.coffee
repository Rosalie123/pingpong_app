App.messages = App.cable.subscriptions.create "MessagesChannel",
  collection: -> $('#messages')
  form: -> $('#new_message')

  connected: ->
    setTimeout =>
      @followCurrentConversation()
      @installPageChangeCallback()
    , 1000

  disconnected: ->

  followCurrentConversation: ->
    conversationId = @collection().data('conversation-id')
    if conversationId
      @perform 'follow', conversation_id: conversationId
    else
      @perform 'unfollow'

  userIsCurrentUser: (message) ->
    $(message).attr('data-user-id') is $('meta[name=current-user]').attr('id')


  received: (data) ->
    @form().after(data['message']) unless @userIsCurrentUser(data.message)



  installPageChangeCallback: ->
    unless @installedPageChangeCallback
      @installedPageChangeCallback = true
      $(document).on 'turbolinks:load', -> App.messages.followCurrentConversation()
