App.notifications = App.cable.subscriptions.create "NotificationsChannel",

  connected: ->

  disconnected: ->

  received: (data) ->
    initnum = $('.notification-number')
    initnum.show().text(data['count'])
    $('.notification-list').prepend(data['notification'])
