$(document).on("turbolinks:load", function(){
  $('.click-dropdown').children('a').click(function(event){
    event.preventDefault();
    var target = $($(this).parent().data('target'));
    target.slideToggle('fast');
    if ($(this).parent().hasClass('notification') &&
       $('.notification-number').is(':visible')){
      $.ajax ({
        url: '/notifications',
        type: 'PATCH',
        dataType: 'script'
      });
    }
  });
  $(document).click(function(event){
    $('.click-dropdown').each(function(){
      if (!$.contains(this, event.target)){
        $($(this).data('target')).slideUp('fast');
      }
    });
  });
  $('.notification').on('click', 'li', function(){
    if ($(this).hasClass('unread')){
      $.ajax ({
        url: '/notifications/' + $(this).data("notification"),
        type: 'PATCH',
        dataType: 'script'
      });
      $(this).removeClass('unread');
    }
  });
});
