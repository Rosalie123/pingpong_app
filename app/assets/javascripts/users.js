// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

var placeholder
var current_address

$(document).on("turbolinks:load", function(){
  placeholder = $('.description').text();
  current_address = $('.current_address').text();
  $('.user-description').on('click', '#description', function(event){
    event.preventDefault();
    $('.description').html("<textarea class='text-des'>" + placeholder + "</textarea>" +
        "<a id='des-update' class='btn btn-primary btn-signup' href='#'>Update</a>");
    $('#description').replaceWith("<a id='des-cancel' class='btn btn-secondary' href='#'>Cancel</a>");
  });

  $('.user-description').on('click', '#des-update', function(event){
    event.preventDefault();
    var params = {
      user: {
        description: $('.text-des').val()
      }
    };
    $.ajax ({
      url: '/users/' + $('.user-basic').data('userid'),
      type: 'PATCH',
      dataType: 'script',
      data: params
    })
  });

  $('.user-description').on('click', '#des-cancel', function(event){
    event.preventDefault();
    $('.description').html("<p>" + placeholder + "</p>");
    $('#des-cancel').replaceWith("<a id='description' class='link-primary' href='#'>edit</a>");
  });

  $('#location').click(function(event){
    event.preventDefault();
    $('.current_address').html("<input row=1 width=60px id='new_location'></input>" +
      "<a id='loc-update' class='explanation link-primary' href='#'>Update</a>"
    );
    $('#location').hide();
  });

  $('.location').on('click', '#loc-update', function(event){
    event.preventDefault();
    var params = {
      user: {
        location: $('#new_location').val()
      }
    };
    $.ajax ({
      url: '/users/' + $('.user-basic').data('userid'),
      type: 'PATCH',
      dataType: 'script',
      data: params
    })
  })

  $('.location').on('click', '#new_location', function(event){
    event.preventDefault();
    completeLocation();
  });

  function completeLocation(){
    var input = (document.getElementById('new_location'));
    var autocomplete = new google.maps.places.Autocomplete(input);
  };

  $('#message-me').click(function(event){
    event.preventDefault();
    var receiver_id = $('.user-basic').data('userid')
      $.ajax ({
        url: '/conversations/new',
        data: {
            receiver_id: receiver_id
        },
        type: 'POST',
        dataType: 'script'
      });
    });

  $('.content').on('click', '.exit', function(event){
    event.preventDefault();
    $('.pop-up-conversation').fadeOut();
  });

});
