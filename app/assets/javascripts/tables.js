// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on("turbolinks:load", function(){
  $('#btn-popup').click(function(event){
    event.preventDefault();
    $('.pop-up-photo').fadeIn();
  });
  $('.around_me').click(function(event){
    event.preventDefault();
    sendLocation();
    var fetch = $('<span>fetching tables</span>');
    $(this).replaceWith(fetch);
    var n = 0;
    (function loadingDot(){
        if (n == 3){
          fetch.text('fetching tables');
        } else {
          fetch.text(fetch.text()+' .')
        }
        n = (n + 1)%4
        setTimeout(loadingDot, 500)
    })();
  });
  rateTable();
  showEditRating();
});

function rateTable(){
  $('.user_rating').each(function(){
    var hoverclose = true
    var self = this
    $(self).mouseenter(function(){
      hoverclose = true
    });
    $(self).children('span').hover(function(){
      if (hoverclose) {
        $(this).add($(this).prevAll()).addClass('green');
      }
    }, function(){
      if (hoverclose){
        $(self).children('span').removeClass('green');
      }
    }).click(function(){
        $(self).prev()[0].value = $(this).attr('data-id');
    });
    $(self).mouseleave(function(){
      var id = $(self).prev()[0].value;
      if (id){
        var rate = $(self).find('[data-id='+ parseInt(id) + ']');
        rate.add(rate.prevAll()).addClass('green');
      }
    });
  });
}
function showEditRating(){
  $('.rating-edit').click(function(event){
    event.preventDefault();
    $('.pop-up-rating').fadeIn();
    $.getScript($('.table_info').attr('data-tableid') + '/ratings/' +
                            $(this).attr('data-ratingId') + '/edit');
  });
}
