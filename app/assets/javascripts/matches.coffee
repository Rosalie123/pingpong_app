# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
  $('.calendar').on 'click', '.close', (event) ->
    event.preventDefault()
    $(".matchinfo-wrap").fadeOut()
    $(".alert").remove()
  $('.calendar').on 'click', '#remove-new-message', (event) ->
    event.preventDefault()
    $(".matchinfo-wrap").remove()
