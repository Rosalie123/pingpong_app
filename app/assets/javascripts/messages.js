// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on("turbolinks:load", function(){
   $('#filter').change(function(){
      var params = {
        filter: $('#filter').val()
      };
      $.ajax ({
        url: 'conversations',
        data: params,
        type: 'GET',
        dataType: 'script'
      });
   });
   $('.load-message').click(function(event){
     event.preventDefault();
     var params = {
       index: $(this).index('.load-message')
     };
     $.ajax({
       url: $(this).next().find('.next_page').attr('href'),
       data: params,
       type: 'GET',
       dataType: 'script'
   });
 });
});
