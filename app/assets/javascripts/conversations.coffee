# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
  $('.conversation').click ->
    $(this).removeClass('unread')
  $(window).scroll ->
    url = $('.conversations-pagecount .pagination .next_page').attr('href')
    if url && $(window).scrollTop() > $(document).height() - $(window).height() - 50
      $('.pagination').html("")
      $('.conversations-notice').html("<h3>Fetching more conversations...</h3>")
      $.ajax
        url: url,
        type: 'Get',
        dataType: 'script',
        data: {filter: $('#filter').val()}
