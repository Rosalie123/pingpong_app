// showing selected single table

function initMap() {
   var map = new google.maps.Map(document.getElementById('map'), {
     zoom: 13,
     center: tableCor
   });
   var marker = new google.maps.Marker({
     position: tableCor,
     map: map
   });
   initAutocomplete();
}

//  Initial Autocomplete

function initAutocomplete() {
  var input = (document.getElementById('autocomplete'));
  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    if (place.geometry.viewport){
      window.location.href = '/tables?bound=' + place.geometry.viewport.toUrlValue()
                              +'&center=' + [lat, lng]
    }
    else {
      window.location.href = '/tables?search=' + [lat, lng]
    }
  });
}


// Return search result on maps (action: index)

var markers = [];
var map;
var preMarker;
var infoAutopan = true;


function mapResult() {
  var secondchange = false;
    map = new google.maps.Map(document.getElementById('map'), {
       zoom: 13,
       center: center
   });
   var bounds;
   if (res_bound) {
     var  sw = new google.maps.LatLng( res_bound[0], res_bound[1] ),
          ne = new google.maps.LatLng( res_bound[2], res_bound[3] );
     bounds = new google.maps.LatLngBounds(sw, ne);
     map.fitBounds(bounds);
   } else {
     bounds = new google.maps.LatLngBounds();
   }
   results.forEach(function(result, i) {
     var marker = new google.maps.Marker({
       position: result,
       map: map,
       id: i
     });
     addInfoWindow(marker, i)
     markers.push(marker);
     bounds.extend(result);
   });
   map.fitBounds(bounds);
   initAutocomplete();
   jumpingMarker();

    map.addListener('idle', function(){
      if ((autofresh || secondchange) && (infoAutopan)){
        bounds = map.getBounds();
        center = map.getCenter();
        zoom = map.getZoom();
        $.ajax ({
           type: 'Get',
           dataType: 'script',
           url: '/tables?bound=' + bounds.toUrlValue() + '&center=' +
                [center.lat(), center.lng()] + '&zoom=' + zoom + '&dragged=' + true
       });
     } else {
       secondchange = true;
       infoAutopan = true;
     }
  });

  function jumpingMarker(){
    var jumpingMarker;
    $('.tables').on('mouseenter','.frame-index',function(){
        var index = $('.frame-index').index(this)
        markers.forEach(function(marker) {
          if (marker.get("id") === index){
            jumpingMarker = marker;
            marker.setAnimation(google.maps.Animation.BOUNCE);
          return;
          }
        });
      }).on('mouseleave', '.frame-index', function(){
          jumpingMarker.setAnimation(null);
      });
  }

}

function addInfoWindow(marker, i){
  marker.info = new google.maps.InfoWindow({
    content: infowindowarray[i]
  });
  marker.addListener('click', function(){
    if (preMarker){
      preMarker.info.close();
    }
    preMarker = marker;
    marker.info.open(mapNew,marker);
    marker.info.addListener('domready',function(){
      $('#map').find('.carousel-index').slick();
    });
    infoAutopan = false;
  });
}


//  showing current location in initiated map (action: index around me)

function sendLocation(){
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(success, error);
  }
  else {
    // Browser doesn't support Geolocation
    x.innerHTML="Geolocation is not supported by this browser."
  }
}

function error(e) {
  console.log("error code:" + e.code + 'message: ' + e.message );
}

function success(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  window.location.href = '/tables?search=' + [lat, lng]
}

//  create a table (action: new)

var mapNew;
var marker;
var icon;

function getLocation(){
  initAutocomplete();
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successset, error);
  }
  else {
    x.innerHTML="Geolocation is not supported by this browser."
  }
}

function successset(position) {
  icon = 'http://maps.google.com/mapfiles/marker_green.png';
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  var myLocation = new google.maps.LatLng(lat, lng);
  mapNew = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center:myLocation
  });
  marker = new google.maps.Marker({
    position: myLocation,
    draggable: true,
    map: mapNew,
    icon: icon,
    animation: google.maps.Animation.DROP,
  });

  $('#newpin_adr').click (event, function(){
    event.preventDefault()
    changePin();
  });

  loadMarker();
  showInfo();
  saveCoor();

  function changePin(){
    var input = (document.getElementById('newpin_adr'));
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      var position = place.geometry.location;
      infowindow.setContent('Double click to save the location');
      mapNew.setCenter(position);
      marker.setMap(null);
      marker = new google.maps.Marker({
        position: position,
        draggable: true,
        icon: icon,
        map: mapNew,
        animation: google.maps.Animation.DROP,
      });
      infowindow.open(mapNew,marker);
      saveCoor();
    });
  }

  function showInfo() {
    var contentString = "This is your current location, drag the pin to where the"
    + " table is or use the search bar to drop a pin"

    infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    infowindow.open(mapNew, marker);
    marker.addListener('drag', function() {
       infowindow.setContent('Double click to save the location');
       infowindow.open(mapNew,marker);
    });
  }

  function saveCoor() {
    marker.addListener('dblclick', function() {
      document.getElementById('newpin_lat').value = marker.getPosition().lat();
      document.getElementById('newpin_lng').value = marker.getPosition().lng();
      infowindow.setContent('Location saved');
      infowindow.open(mapNew,marker);
    });
  }

  function loadMarker() {
    mapNew.addListener('idle', function(){
      var bounds = new google.maps.LatLngBounds();
      bounds = mapNew.getBounds();
      $.ajax ({
         type: 'Get',
         dataType: 'script',
         url: '/tables/new?myBound=' + bounds.toUrlValue()
     });
   });
  }

}
