var initialize_calendar;
initialize_calendar = function(){
  $('.calendar').each(function(){
    var calendar = $(this);
    var selectable = $('#calendar').length;
    calendar.fullCalendar({
      header: {
        left: 'prev, next, today',
        center: 'Arrange a BATMatch',
        right: 'month, agendaWeek, agendaDay, listYear'
      },
      selectable: selectable,
      selectHelper: true,
      editable: true,
      eventLimit: true,
      events: {
                url: '/matches.json',
                data: {
                    user_id: $('.user-basic').data('userid'),
                    table_id: $('#calendar').data('tableid')
                }
      },
      eventDrop: function(event, delta, revertFunc) {
        var match_params = {
          match: {
            id: event.id,
            start_at: event.start.format(),
            end_at: event.end.format()
          }
        };
        $.ajax ({
          url: event.update_url,
          data: match_params,
          type: 'PATCH'
        });
      },
      eventDragStop: function(event,jsEvent) {
        var trashEl = jQuery('#calendarBin');
        var ofs = trashEl.offset();
        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);
        if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 &&
            jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
          var match_params = {
            match: {
              id: event.id,
              start_at: event.start.format(),
              end_at: event.end.format()
            }
          };
          $.ajax ({
            url: event.delete_url,
            data: match_params,
            type: 'DELETE'
          });
        }
      },
      eventRender: function(event, element) {
        element.bind('click', function() {
          $.ajax ({
            url: event.show_url,
            type: 'GET'
          });
        });
      },
      dayClick: function(date, jsEvent, view) {
        if(view.name == 'month' || view.name == 'agendaWeek') {
            $('.calendar').fullCalendar('gotoDate', date);
            $('.calendar').fullCalendar('changeView', 'agendaDay');
        }
      },
      select: function(start, end) {
        var check = start.format('YYYY-MM-DD HH:mm:ss');
        var now = moment().format("YYYY-MM-DD HH:mm:ss");
        var view = $(".calendar").fullCalendar('getView');
        if (view.name == 'agendaDay'){
          if (check > now){
            $.getScript('matches/new')
            $('.start_time').val(moment(start).format("YYYY-MM-DD HH:mm:ss"));
            $('.end_time').val(moment(end).format("YYYY-MM-DD HH:mm:ss"));
          }
          else {
            alert("You can't arrange match before current time")
            calendar.fullCalendar('unselect');
          }
        }
        calendar.fullCalendar('unselect')
      }
    });
  });
};
$(document).on("turbolinks:load", initialize_calendar)
