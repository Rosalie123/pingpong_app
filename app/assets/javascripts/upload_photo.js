$(document).on("turbolinks:load", upload_photo);

function upload_photo(){
  $('#upload_photo').fileupload({
    dataType: 'script',
    add: function(e, data) {
      $('.alert').remove();
      data.context = $(tmpl("template-upload", data.files[0]));
      $('.photo_preview').append(data.context);
      data.submit();
    },
    progress: function (e, data) {
      if (data.context) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        data.context.find('.bar').css('width', progress + '%');
      }
    },
    done: function (e, data) {
      if (data.context) {
        data.context.find('.bar').parent().parent().remove();
        $('.photo_preview').prepend($("<div class='alert alert-success alert-small'>"
                                + "Uploaded successfully!</div>"));
        $('.btn-skip').remove();
      }
    }
  });
};
