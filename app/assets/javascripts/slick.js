
$(document).on("turbolinks:load", function(){
   $('.carousel').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: true,
     fade: true,
     asNavFor: '.carousel-small',
     autoplay: true,
     autoplaySpeed: 2000,
     speed: 1500
   });

  $('.carousel-small').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.carousel',
    dots: true,
    centerMode: true,
    focusOnSelect: true,
  });

  $('.carousel-index').slick();
});


$(document).on('turbolinks:before-cache', function(){
  $('.carousel, .carousel-small, .carousel-index').slick('unslick');
});
