# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
  $('#bat_agent').click (event) ->
    event.preventDefault()
    $('.signup_block').css("display","flex")
    $('html, body').animate({scrollTop: $("#new_user").offset().top}, 1500)
  $('#login').click (event) ->
    event.preventDefault()
    $('.pop-up-login').fadeIn()
  $('.exit').click (event) ->
    event.preventDefault()
    $(".pop-up").fadeOut()
    $(".alert").fadeOut()
    $(".rating-edit-form").hide()
  
