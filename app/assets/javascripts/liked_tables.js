// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on("turbolinks:load", function(){
  likeTable()
});


  function likeTable(){
   $('.heart-hollow').click(function(){
     var tableId = $('.table_info').data('tableid');
     var type;
     var url;
     var params = {
       liked_tables: {
         table_id: tableId
       }
     };
     if ($('.heart-red').is(':visible')){
       type = 'DELETE';
       url = '/liked_tables/' + tableId;
     } else {
       type = 'POST';
       url = '/liked_tables';
     }
     $.ajax({
       url: url,
       type: type,
       dataType: 'script',
       data: params
     });
    });
  }
