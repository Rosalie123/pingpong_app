class MessagesChannel < ApplicationCable::Channel

  def follow(params)
    stop_all_streams
    stream_from "conversations:#{params['conversation_id'].to_i}:messages"
  end

  def unfollow
    stop_all_streams
  end

end
