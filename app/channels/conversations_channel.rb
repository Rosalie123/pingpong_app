class ConversationsChannel < ApplicationCable::Channel

  def follow
    stop_all_streams
    stream_from "conversations:#{current_user.id}"
  end

  def unfollow
    stop_all_streams
  end

end
