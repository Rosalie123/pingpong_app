class NotificationBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message, conversation)
    create_notification(message, conversation)
    ActionCable.server.broadcast "notifications:#{counterpart(message, conversation)}",
                {count: @notification.receiver.received_notifications.where(seen: false).count,
                 notification: render_notification}

  end

  private

  def create_notification(message, conversation)
    if message.notice
      if conversation.match.nil?
        content = "#{message.content}"
        title = 'Match'
      else
        content ="#{message.content} for match at #{conversation.match.render_date}"
        title = "Bid"
      end
    else
      content = "#{message.author.name} sent you a message:
                  #{message.content.first(10)}..."
      title = "Message"
    end
    @notification = conversation.notifications.create(
                      notifier_id: message.author_id,
                      receiver_id: counterpart(message, conversation),
                      content: content,
                      title: title)
  end

  def render_notification
    ApplicationController.render(partial: 'notifications/notification',
                                          locals: {notification: @notification})
  end

end
