class TableBroadcastJob < ApplicationJob
  queue_as :default

  def perform(table)
    create_notification(table)
    ActionCable.server.broadcast "notifications:#{table.user_id}",
    {count: @notification.receiver.received_notifications.where(seen: false).count,
     notification: render_notification}
  end

  private

  def create_notification(table)
    @notification = table.user.received_notifications.create(
                      content: "You have earned a new badge for finding the table at #{table.address}",
                      title: 'BATBadge')
  end
end
