class RatingBroadcastJob < ApplicationJob
  queue_as :default

  def perform(rating)
    create_notification(rating)
    ActionCable.server.broadcast "notifications:#{rating.user_id}",
    {count: @notification.receiver.received_notifications.where(seen: false).count,
     notification: render_notification}
  end

  private

  def create_notification(rating)
    @notification = rating.user.received_notifications.create(
                      content: "You have earned a new star for rating the table at #{rating.table.address}",
                      title: 'BATStar')
  end
end
