class ApplicationJob < ActiveJob::Base

  def counterpart(message, conversation)
    message.author_id == conversation.sender_id ? conversation.receiver_id :
                                                  conversation.sender_id
  end

  def render_notification
    ApplicationController.render(partial: 'notifications/notification',
                                          locals: {notification: @notification})
  end
end
