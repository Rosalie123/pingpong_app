class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message, conversation)
    unread_message(message, conversation)
    ActionCable.server.broadcast "conversations:#{conversation.id}:messages",
                                            { message: render_message(message)}

    ActionCable.server.broadcast "conversations:#{counterpart(message,conversation)}",
                              conversation_id: conversation.id,
                              message: message.content,
                              status:  conversation.status,
                              date:   message.render_date

  end

  private

  def render_message(message)
    MessagesController.render(partial: 'messages/message',
        locals: {message: message, var: 'not'})
  end

  def unread_message(message, conversation)
    if message.author_id == conversation.receiver_id
      conversation.update_attributes(read_receiver: true,
                                              read_sender: false)
    else
      conversation.update_attributes(read_sender: true,
                                              read_receiver: false)
    end
  end

end
