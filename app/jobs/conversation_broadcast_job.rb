class ConversationBroadcastJob < ApplicationJob
  queue_as :default

  def perform(conversation)
    create_message(conversation) unless conversation.match_id.nil?
    ActionCable.server.broadcast "conversations:#{conversation.receiver_id}",
                                  {convo: render_conversation(conversation)}
  end

  private

  def render_conversation(conversation)
    ConversationsController.render(partial: 'conversations/test',
                                        locals: {conversation: conversation})
  end

  def create_message(conversation)
    @message =  conversation.messages.create(
                      author_id: conversation.sender_id,
                      notice: true,
                      content: "#{conversation.sender.name} put on a bid")
  end

end
