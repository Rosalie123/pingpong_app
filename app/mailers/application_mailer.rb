class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@BATMAP.com'
  layout 'mailer'
end
