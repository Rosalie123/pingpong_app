class UserMailer < ApplicationMailer

  def user_activate(user)
    @user = user
    mail to: user.email, subject: "BATMAP: One more step to become a BAT agent!"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "BATMAP: Password Reset"
  end

end
