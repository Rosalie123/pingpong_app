class TablesController < ApplicationController

  before_action :get_table, only: [:show]
  before_action :logged_in_user, only: [:new, :create]
  before_action :current_user, only: [:new, :create, :show]
  before_action :rated_user, only: [:show]
  before_action :table_liker, only: [:show]
  before_action :admin_user?, only: [:destory]

  def index
    @autofresh = false
    if params[:search].present?
      @tables = Table.within(5, origin: params[:search]).by_distance(
                             origin: params[:search]).paginate(
                             page: params[:page], per_page: 10)
      coordinate_gmap(params[:search])

    elsif (params[:bound].present?) && (params[:center].present?)
      @bounds = params[:bound].split(",").map { |b| b.to_f}
      coordinate_gmap(params[:center])
      bound = params[:bound].split(",").map { |b| b.to_f }.each_slice(2).to_a
      if params[:dragged].present?
        @tables = Table.in_bounds(bound).by_distance(origin: params[:center]).paginate(
                                                    page: params[:page],
                                                    per_page: 10)
      else
        @tables = Table.in_bounds(bound).all.order(overal_rating: :desc).paginate(
                                                  page: params[:page],
                                                  per_page: 10)
      end

      if (@tables.empty?) && (!params[:dragged].present?)
          @tables = Table.by_distance(origin: params[:center]).paginate(
                                                  page: params[:page],
                                                  per_page: 1, total_entries: 1)
          @autofresh = true
      end
    else
      @tables = Table.all.order(overal_rating: :desc).paginate(page: params[:page], per_page: 10)
      @center = {lat: 48.8534, lng: 2.3488}
    end

    load_markers(@tables)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    get_table_cor(@table)
    @comments = @table.comments.paginate(page: params[:page], per_page: 5)
    @pictures = @table.pictures.all
    @best = Comment.best(@table)
  end

  def new
    @table = current_user.tables.build
    @picture = @table.pictures.build
    @comment = @table.comments.build
    @rating = @table.ratings.build
    if params[:myBound].present?
      bound = params[:myBound].split(",").map { |b| b.to_f }.each_slice(2).to_a
      @tables = Table.in_bounds(bound).all
      load_markers(@tables)
    end
    respond_to do |format|
      format.js
      format.html
    end
  end


  def create
    @table = @current_user.tables.build(table_params)
    if @table.save
      @rating = @table.ratings.build
      @rating.update_rating(@table)
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def destroy
    @table = Table.find(params[:id]).destroy
    redirect_to root_url
  end

  # def edit
  #   @table = Table.find_by(id: params[:id])
  #   get_table_cor(@table)
  # end
  #
  # def update
  #   @table = Table.find_by(id: params[:id])
  #   @table.update_attributes(table_params)
  # end




  private

  def get_table
    @table = Table.find_by(id: params[:id]) or render_404
  end

  def rated_user
    if @current_user
      @table.ratings.each do |rating|
        if rating.user_id == @current_user.id
          @rating = rating
        end
      end
    end
  end

  def table_params
    params.require(:table).permit(:parking, :latitude, :longitude,
                                  :overal_rating, :availability_rating,
                                  :surrounding_rating, :table_rating,
                                  :ground_rating,:toilet, :playground, :picnic,
                                  :table_number)
  end

  # get coordinate for a table selected
  def get_table_cor(table)
    @table_cor = {
      lat: table.latitude,
      lng: table.longitude
    }
  end

# transfer to array

  def coordinate_gmap(params)
    array = params.split(",").map { |b| b.to_f }
    @center = {
      lat: array[0],
      lng: array[1]
    }
  end

  def geocode(params)
    geo=Geokit::Geocoders::MultiGeocoder.geocode(params)
    errors.add(:address, "Could not Geocode address") if !geo.success
    @search = [geo.lat, geo.lng] if geo.success
  end

  def load_markers(tables)
    @results = []
    @infowindowarray = []
    tables.each do |table|
      @results << get_table_cor(table)
      infowindow = view_context.render(partial: 'tables/table', locals: {table: table})
      @infowindowarray << infowindow
    end
  end



end
