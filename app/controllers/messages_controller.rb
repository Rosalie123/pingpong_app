class MessagesController < ApplicationController

  before_action :logged_in_user
  before_action :current_user
  before_action :get_conversation
  before_action :correct_participant

  def create
    @message = @conversation.messages.build(message_params)
    @message.author_id = @current_user.id
    @message.save
  end

  def index
    @messages = @conversation.messages.order('created_at DESC')
                             .paginate(page: params[:page],
                                       per_page: 8)
    read_message
    get_participant
    unless  request.xhr?
      @match = @conversation.match
      @bid = @conversation.bid unless @conversation.bid.nil?
      @table = @match.table unless @match.nil?
    end
  end

  private

  def message_params
    params.require(:message).permit(:author_id, :conversation_id, :content)
  end

  def get_conversation
    @conversation = Conversation.find_by(id:  params[:conversation_id])
  end

  def get_participant
    current_user.id == @conversation.sender.id ? @participant = @conversation.receiver :
                                                 @participant = @conversation.sender
  end

  def correct_participant
    redirect_to root_url unless
    (@conversation.sender == current_user) || (@conversation.receiver == current_user)
  end

  def read_message
    last_msg = @conversation.last_msg
    if (!last_msg.nil?) && (@current_user == @conversation.sender)
      @conversation.update_attributes(read_sender: true)
    elsif (!last_msg.nil?) && (@current_user == @conversation.receiver)
      @conversation.update_attributes(read_receiver: true)
    elsif (last_msg.nil?) && (@current_user == @conversation.receiver)
      @conversation.update_attributes(read_receiver: true)
    end
  end

end
