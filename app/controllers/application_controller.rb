class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  # Confirms a logged-in user.
def logged_in_user
  unless logged_in?
    flash[:danger] = 'Please log in!'
    respond_to do |format|
      format.html {redirect_to login_url}
      format.js { render js: "window.location = '/login'"}
    end
  end
end


def table_liker
  if @current_user
    @table.likers.each do |liker|
      if liker.liker_id == @current_user.id
        @liker = liker
      end
    end
  end
end

def render_404
  respond_to do |format|
    format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
    format.xml  { head :not_found }
    format.any  { head :not_found }
  end
end

end
