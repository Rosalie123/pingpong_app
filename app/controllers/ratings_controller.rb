class RatingsController < ApplicationController

  before_action :logged_in_user
  before_action :current_user
  before_action :get_table, only: [:new, :create, :update]
  before_action :already_rated?, only: [:new, :create]
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :table_liker, only: [:update]

  def new
    @rating = @current_user.ratings.build
    @pictures = @table.pictures
  end

  def create
    @rating = @current_user.ratings.build(ratings_params)
    if @rating.save
      @table.update_rating(@rating)
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def edit
  end

  def update
    @rating.update_attributes(ratings_params)
    @rating.table_id = params[:table_id]
    if @rating.save
      @table.update_rating(@rating)
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def destroy
  end

  private

  def ratings_params
    params.require(:rating).permit(:availability_rating,
                                   :surrounding_rating,
                                   :table_rating,
                                   :ground_rating,
                                   :table_id)
  end


  def get_table
    @table = Table.find_by(id: params[:table_id])
  end

  def already_rated?
    @table.ratings.each do |rating|
      if rating.user_id == @current_user.id
        flash[:warning] = "You've already rated this table!"
        redirect_to table_path(@table)
        return
      end
    end
  end

  def correct_user
    @rating = @current_user.ratings.find(params[:id])
    redirect_to root_url if @rating.nil?
  end


end
