class UserActivationsController < ApplicationController
  include SessionsHelper
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation,
                                                        params[:id])
       user.activate
       log_in user
       remember(user)
       flash[:success] = "Welcome #{user.name}, you're a verified BATAgent now!"
       redirect_to root_path
    else
       flash[:danger] = "Invalid activation link"
       redirect_to root_path
    end
  end

end
