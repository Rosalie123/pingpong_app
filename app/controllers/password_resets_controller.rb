class PasswordResetsController < ApplicationController

  before_action :user_exist, only: [:edit, :update]
  before_action :user_valid, only: [:edit, :update]
  before_action :check_expire, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      if @user.activated?
         @user.create_password_reset_digest
         @user.send_password_reset_email
         flash[:info] = "Password reset email has been sent to your email adddress"
         redirect_to root_url
      else
        flash.now[:warning] = "You haven't activated your account,
        please go to your email and activate your account"
        render 'new'
      end
    else
      flash.now[:warning] = "Email address doesn't exist"
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, "can't be empty")
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "Password has been reset."
      @user.delete_reset_digest
      redirect_to root_url
    else
      render 'edit'
    end
  end


  private

  def user_exist
    @user = User.find_by(email: params[:email])
  end

  def user_valid
    unless @user && @user.activated? && @user.authenticated?(:reset, params[:id])
      flash[:danger] = "Password reset failed!"
      redirect_to root_url
    end
  end

  def check_expire
    if @user.password_reset_expired
      flash[:warning] = "Password reset is expired, please try again"
      redirect_to new_password_reset_path
    end
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

end
