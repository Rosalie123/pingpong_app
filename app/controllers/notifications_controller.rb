class NotificationsController < ApplicationController
    before_action :logged_in_user
    before_action :current_user

  def seen
    @notifications = @current_user.received_notifications
    @notifications.update_all(seen: true)
  end

  def update
    @notification = Notification.find(params[:id])
    if @notification.conversation_id.nil?
      @notification.update_attributes(read: true)
    else
      @notification.conversation.notifications.where(receiver_id: @current_user.id)
                   .update_all(read: true)
    end
  end

end
