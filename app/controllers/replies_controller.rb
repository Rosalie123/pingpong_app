class RepliesController < ApplicationController

  before_action :logged_in_user
  before_action :current_user
  before_action :get_comment


  def new
    @reply = @comment.replies.build
  end

  def create
    @reply = @current_user.comments.create(reply_params)
  end

  private

  def reply_params
    params.require(:comment).permit(:table_id, :user_id, :content, :parent_id, :title)
  end

  def get_comment
    @comment = Comment.find_by(id: params[:comment_id])
    redirect_to root_url if @current_user == @comment.user
  end

end
