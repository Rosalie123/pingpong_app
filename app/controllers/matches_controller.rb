class MatchesController < ApplicationController
    before_action :logged_in_user
    before_action :current_user
    before_action :get_table, only: [:create]
    before_action :correct_user, only: [:destroy]

  def new
    @match = @current_user.proposed_matches.build
  end

  def index
    @table = Table.find_by(id: params[:table_id])
    if request.xhr?
      if !params[:table_id].nil?
        @confirmed_matches = Match.proposer_not_me(@current_user).accepter_not_me(
                            @current_user).this_table(
                            @table).time_slot(params[:start], params[:end]).confirmed

        @mine_confirmed = Match.proposer_me(@current_user).this_table(
                            @table).time_slot(params[:start], params[:end]).confirmed

        @mine_accepted = Match.accepter_me(@current_user).this_table(
                            @table).time_slot(params[:start], params[:end]).confirmed

        @i_bidded_matches = Match.this_table(@table).time_slot(params[:start],
                            params[:end]).i_bidded(@current_user).proposed

        @proposed_matches = Match.proposer_not_me(
                            @current_user).this_table(@table)
                            .time_slot(params[:start], params[:end])
                            .proposed.where.not(id: @i_bidded_matches.collect(&:id))

        @mine_proposed = Match.proposer_me(@current_user).this_table(
                            @table).time_slot(params[:start], params[:end]).proposed

      elsif !params[:user_id].nil?
        @user = User.find_by(id: params[:user_id])
        @mine_confirmed = Match.participant_me(@user)
                              .time_slot(params[:start], params[:end]).confirmed
        @mine_proposed_user = Match.proposer_me(@user)
                            .time_slot(params[:start], params[:end]).proposed
        if @user == @current_user
          @i_bidded_matches = Match.time_slot(params[:start],
                              params[:end]).i_bidded(@current_user).proposed
        end
      end
    end
    respond_to do |format|
      format.html
      format.json
    end
  end

  def show
    @match = Match.find_by(id: params[:id])
  end

  def create
    @match = @current_user.proposed_matches.build(match_params)
    @match.save
  end

  def update
    @match = Match.find_by(id: params[:id])
    @match.update_attributes(match_params)
  end

  def destroy
    @match.destroy
  end


  private

  def match_params
    params.require(:match).permit(:proposer_id, :accepter_id, :table_id,
                                  :confirmed, :start_at, :end_at)
  end

  def get_table
    @table = Table.find(params[:table_id])
  end


  def correct_user
    @match = @current_user.proposed_matches.find_by(id: params[:id])
    redirect_to root_url if @match.nil?
  end

end
