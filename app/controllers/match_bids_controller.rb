class MatchBidsController < ApplicationController

  before_action :logged_in_user
  before_action :current_user
  before_action :correct_user, only: [:destroy]
  before_action :get_match


  def create
    @bid = @current_user.bids.create(bid_params)
  end

  def destroy
    @bid.destroy
  end

  def update
    if @current_user.id == @match.proposer_id
      @bid = @match.match_bids.find_by(bidder_id: params[:bid][:bidder_id])
      @bid.update_attributes(bid_params)
      respond_to do |format|
        format.html { redirect_to user_path(@current_user)}
        format.js
      end
    else
      redirect_to root_url
    end
  end


  private
    def get_match
      @match = Match.find(params[:match_id])
    end

    def bid_params
      params.require(:bid).permit(:accepted, :valid, :match_id)
    end

    def correct_user
      @bid = @current_user.bids.find_by(id: params[:id])
      redirect_to root_url if @bid.nil?
    end

end
