class CommentsController < ApplicationController

  before_action :logged_in_user
  before_action :current_user
  before_action :correct_user, only: [:edit, :destroy, :update]

  def create
    @comment = @current_user.comments.create(comment_params)
  end

  def edit
    @table = @comment.table
  end

  def destroy
    @comment.destroy
  end

  def update
    @comment.update_attributes(comment_params)
  end

  private

  def comment_params
    params.require(:comment).permit(:table_id, :user_id, :content, :title)
  end

  def correct_user
    @comment = @current_user.comments.find_by(id: params[:id])
    redirect_to root_url if @comment.nil?
  end

end
