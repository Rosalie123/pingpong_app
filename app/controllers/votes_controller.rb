class VotesController < ApplicationController
  before_action :logged_in_user
  before_action :current_user
  before_action :correct_user, only: [:destroy]

  def create
    @vote = @current_user.votes.build
    @comment = Comment.find(params[:comment_id])
    @vote.comment_id = params[:comment_id]
    @vote.save
  end

  def destroy
    @vote.destroy
  end

  private

  def correct_user
    @vote = @current_user.votes.find_by(id: params[:id])
    @comment = Comment.find(params[:comment_id])
    redirect_to root_path if @vote.nil?
  end

end
