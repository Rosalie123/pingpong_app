class ConversationsController < ApplicationController

  before_action :logged_in_user
  before_action :current_user

  def new
    @receiver = User.find_by(id: params[:receiver_id])
    @conversation = Conversation.new
    @conversation.messages.build
  end

  def index
    if !request.xhr?
      @conversations = Conversation.participating(@current_user)
                                   .paginate(page: params[:page],
                                             per_page: 8)
    elsif params[:filter] != 'my_PM'
      @conversations = Conversation.filter(params[:filter], @current_user)
                                   .paginate(page: params[:page],
                                                    per_page: 8)
    else
      @conversations = Conversation.participating(@current_user).my_PM
                                 .paginate(page: params[:page], per_page: 8)
    end
    @load_more = true unless params[:page].nil?
  end

  def create
    if !params[:bid_id].blank?
      @bid = MatchBid.find(params[:bid_id])
      @match = Match.find(params[:match_id])
      @conversation = @bid.build_conversation
      @conversation.update_attributes(sender_id:   @bid.bidder_id,
                                      receiver_id: @match.proposer_id,
                                      match_id:    @match.id,
                                      status: 'Bid Pending',
                                      subject: "New bid for #{@match.proposer.name}'s match at" +
                                    " #{@match.start_at.to_formatted_s(:short)}")
    else
      @conversation = @current_user.sent_conversations.create(conversation_params)
    end
  end


  private

    def conversation_params
      params.require(:conversation).permit(:sender_id, :receiver_id, :bidder_id, :bid_id,
                                            :subject, :match_id, :status,
                                            messages_attributes: [:content, :author_id])
    end

end
