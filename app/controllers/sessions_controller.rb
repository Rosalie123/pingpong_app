class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        flash[:success] = "Welcome back BAT agent #{@user.name}!"
        respond_to do |format|
          format.js { redirect_to root_path, format: 'js'}
          format.html { redirect_to root_path}
        end
      else
        flash.now[:warning] = "Please activate your account"
        respond_to do |format|
          format.js
          format.html {
            flash[:warning] = "Please activate your account"
            redirect_to root_path}
        end
      end
    else
      flash.now[:danger] = "Wrong password/email combination"
      respond_to do |format|
        format.html {
          flash[:danger] = "Wrong password/email combination"
          render 'sessions/new'}
        format.js
      end
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end
