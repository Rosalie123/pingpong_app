class UsersController < ApplicationController

  before_action :logged_in_user, only: [:show, :update]
  before_action :current_user, only: [:show, :update]
  before_action :get_user, only: [:show]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash.now[:info] = "Thanks for signing up to BATMAP, please go to your
                          email to confirm your email address!"
      @user.send_activation_email
      respond_to do |format|
        format.html {
          flash[:info] = "Thanks for signing up to BATMAP, please go to your
                          email to confirm your email address!"
          redirect_to root_path}
        format.js
      end
    else
      respond_to do |format|
        format.html { render 'users/new'}
        format.js
      end
    end
  end

  def show
    if !request.xhr?
      @wish_tables = @user.wish_tables.paginate(page: params[:page], per_page: 3)
      @rated_tables = @user.rated_tables.paginate(page: params[:page], per_page: 3)
    elsif request.xhr? && params[:index] == "0"
      @rated_tables = @user.rated_tables.paginate(page: params[:page], per_page: 3)
      @rated = true
    else
      @wish_tables = @user.wish_tables.paginate(page: params[:page], per_page: 3)
    end
  end

  def update
    if params[:id].to_i == @current_user.id
      @user = @current_user
      @user.update_attributes(user_params)
      @description = true unless params[:user][:description].nil?
    else
      redirect_to root_url
    end
  end


  private

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation,
                                              :email, :location, :description)
  end

  def get_user
    @user = User.find_by(id: params[:id]) or render_404
  end


end
