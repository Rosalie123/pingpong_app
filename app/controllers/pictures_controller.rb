class PicturesController < ApplicationController

  def create
    @table = Table.find_by(id: params[:pictures][:table_id])
    @picture = @table.pictures.create(picture_params)
  end

  def destroy
    @picture = Picture.find(params[:id]).destroy
    respond_to do |format|
      format.js
    end
  end


  private

  def picture_params
    params.require(:pictures).permit(:picture, :table_id)
  end


  def set_table
    @table = current_user.tables.build
  end

  def add_more_pictures(new_pictures)
    pictures = @tables.pictures
    pictures += new_pictures
    @table.pictures = pictures
  end

end
