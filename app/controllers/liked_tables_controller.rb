class LikedTablesController < ApplicationController
  before_action :logged_in_user
  before_action :current_user
  before_action :correct_user, only: [:destroy]

  def create
    @liked_table = @current_user.liked_tables.create(table_params)
  end

  def destroy
    @table.destroy
  end

private

  def table_params
    params.require(:liked_tables).permit(:liker_id, :table_id)
  end

  def correct_user
    @table = @current_user.liked_tables.find_by(table_id: params[:id])
    redirect_to root_path if @table.nil?
  end
end
