
json.array! @proposed_matches, partial: 'matches/match',
          as: :match,locals: {color: 'lightblue', edit: false} unless @proposed_matches.nil?

json.array! @confirmed_matches, partial: 'matches/match',
        as: :match,locals: {color: 'blue', edit: false}  unless @confirmed_matches.nil?

json.array! @mine_confirmed, partial: 'matches/match',
                             as: :match,locals: {color: 'green', edit: false}

json.array! @mine_proposed, partial: 'matches/match',
                            as: :match, locals: {color: 'lightgreen', edit: true} unless @mine_proposed.nil?

json.array! @mine_proposed_user, partial: 'matches/match',
                            as: :match, locals: {color: 'lightgreen', edit: true}

json.array! @i_bidded_matches, partial: 'matches/match',
        as: :match, locals: {color: 'orange', edit: false} unless @i_bidded_matches.nil?

json.array! @mine_accepted, partial: 'matches/match',
              as: :match, locals: {color: 'red', edit: false} unless @mine_accepted.nil?
