json.ignore_nil!
if match.accepter_id.blank?
  json.title    'Proposer: ' + match.proposer.name
else
  json.title    'Proposer: ' + match.proposer.name + ' - Accepter: ' + match.accepter.name
end
json.id               match.id
json.start            match.start_at
json.end              match.end_at
if match.start_at <= Time.now
  json.color          'lightgray'
else
  json.color           color
end
json.overlap          false
if match.start_at <= Time.now
  json.editable        false
elsif @user.nil?
  json.editable        edit
elsif @user == current_user
  json.editable        edit
else
  json.editable        false
end

if !@table.nil?
  json.update_url       table_match_path(table_id: @table.id, id: match.id, method: :patch)
  json.delete_url       table_match_path(table_id: @table.id, id: match.id, method: :delete)
else
  json.update_url       match_path(id: match.id, method: :patch)
  json.delete_url       match_path(id: match.id, method: :delete)
end
json.show_url         match_path(id: match.id)
