class ConversationPresenter < BasePresenter

  presents :conversation
  delegate :subject, to: :conversation
  delegate :status, to: :conversation


  def name
    if conversation.sender_id == h.current_user.id
      conversation.receiver.name
    else
      conversation.sender.name
    end
  end

  def last_date
    if conversation.last_msg.nil?
      conversation.render_date
    else
      conversation.last_msg.render_date
    end
  end

  def last_message
    conversation.last_msg.content unless conversation.last_msg.nil?
  end

  def avatar
    if conversation.sender_id == h.current_user.id
      h.gravatar_for(conversation.receiver, 30)
    else
      h.gravatar_for(conversation.sender, 30)
    end
  end

  def css
    if ((h.current_user == conversation.receiver) && (!conversation.read_receiver?))
        'unread'
    elsif ((h.current_user == conversation.sender) && (!conversation.read_sender?))
        'unread'
    end
  end

  def sender_id
    if conversation.last_msg.nil?
      conversation.sender_id
    else
      conversation.last_msg.author_id
    end
  end


end
