class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.references :notifier, index: true
      t.references :receiver, index: true
      t.references :conversation, index: true
      t.references :bid, index: true
      t.string    :content
      t.string    :title
      t.boolean   :read, default: false
      t.boolean   :seen, default: false
      t.timestamps
    end
  end
end
