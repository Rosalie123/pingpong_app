class CreateTables < ActiveRecord::Migration[5.0]
  def change
    create_table :tables do |t|
      t.float :latitude
      t.float :longitude
      t.references :user, foreign_key: true
      t.float :table_rating
      t.float :surrounding_rating
      t.float :availability_rating
      t.float :overal_rating
      t.float :ground_rating
      t.string :table_number
      t.boolean :parking
      t.boolean :toilet
      t.boolean :picnic
      t.boolean :playground
      t.string :address
      t.timestamps
    end
    add_index :tables, [:latitude, :longitude]
    add_index :tables, [:user_id, :created_at]
    add_index :tables, [:table_rating]
    add_index :tables, [:surrounding_rating]
    add_index :tables, [:availability_rating]
    add_index :tables, [:overal_rating]
    add_index :tables, [:ground_rating]
  end
end
