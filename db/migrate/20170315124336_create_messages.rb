class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.references :author, index: true
      t.references :conversation, index: true
      t.boolean    :notice, default: false
      t.text       :content
      t.timestamps
    end
  end
end
