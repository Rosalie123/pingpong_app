class CreateMatchBids < ActiveRecord::Migration[5.0]
  def change
    create_table :match_bids do |t|
        t.references :match, foreign_key: true
        t.integer    :bidder_id
        t.boolean    :accepted, default: false
        t.boolean    :valid_bid, default: true
      t.timestamps
    end
    add_index :match_bids, [:bidder_id, :accepted]
    add_index :match_bids, :bidder_id
    add_index :match_bids, :valid_bid
  end
end
