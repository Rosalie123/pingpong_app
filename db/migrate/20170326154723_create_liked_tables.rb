class CreateLikedTables < ActiveRecord::Migration[5.0]
  def change
    create_table :liked_tables do |t|
      t.references :table, index: true
      t.references :liker, index: true
      t.timestamps
    end
  end
end
