class CreateConversations < ActiveRecord::Migration[5.0]
  def change
    create_table :conversations do |t|
      t.references :sender, index: true
      t.references :receiver, index: true
      t.references :bid, index: true, unique: true
      t.references :match, index: true
      t.string :subject
      t.boolean :read_sender, default: true
      t.boolean :read_receiver, default: false
      # t.boolean :personal, default: false
      t.string :status
      t.timestamps
    end
  end
end
