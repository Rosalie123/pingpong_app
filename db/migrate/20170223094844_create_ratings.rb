class CreateRatings < ActiveRecord::Migration[5.0]

  def change
    create_table :ratings do |t|
      t.references :table, foreign_key: true
      t.references :user, foreign_key: true
      t.float :surrounding_rating
      t.float :availability_rating
      t.float :table_rating
      t.float :ground_rating
      t.float :overal_rating
      t.timestamps
    end
  end

end
