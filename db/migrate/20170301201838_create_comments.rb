class CreateComments < ActiveRecord::Migration[5.0]

  def change
    create_table :comments do |t|
      t.references :table, foreign_key: true
      t.references :user, foreign_key: true
      t.string :title
      t.text :content
      t.integer :parent_id
      t.timestamps
    end
    add_index :comments, [:parent_id]
  end
end
