class CreateMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :matches do |t|
      t.integer :proposer_id
      t.integer :accepter_id
      t.references :table, foreign_key: true
      t.datetime :start_at
      t.datetime :end_at
      t.boolean  :confirmed, default: false
      t.timestamps
    end
      add_index :matches, :proposer_id
      add_index :matches, :accepter_id
      add_index :matches, [:accepter_id, :proposer_id]
      add_index :matches, [:start_at, :end_at]
      add_index :matches, [:start_at, :end_at, :table_id]
      add_index :matches, [:start_at, :end_at, :proposer_id]
  end
end
