# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# # movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# # Character.create(name: 'Luke', movie: movies.first)
#
#
# # Users
# User.create!(name:  "Yaoie",
#              email: "example@chao.com",
#              password:              "password",
#              password_confirmation: "password",
#              admin: true,
#              activated: true,
#              activated_at: Time.zone.now)
#
#  10.times do |n|
#    name  = Faker::Name.name
#    email = "example-#{n+1}@chao.com"
#    password = "password"
#    User.create!(name:  name,
#                 email: email,
#                 password:              password,
#                 password_confirmation: password,
#                 activated: true,
#                 activated_at: Time.zone.now)
#  end
#
#  # Tables
#  user = User.first
#  15.times do |n|
#    latitude = 48.852 + ((n.to_f+1)/200)
#    longitude = 2.349 + ((n.to_f-1)/400)
#    Table.create!(user_id: user.id,
#                  latitude: latitude,
#                  longitude: longitude,
#                  parking: true,
#                  pictures: File.open(File.join(Rails.root, "/app/assets/images/background.jpg")),
#                  overal_rating:10,
#                  table_rating: 9,
#                  surrounding_rating: 8,
#                  availability_rating: 10,
#                  ground_rating: 9,
#                  toilet: true,
#                  playground: true,
#                  picnic: true,
#                  )
#  end
#
#  #comments
#  user = User.first
#  table = Table.first
#  content = Faker::Lorem.sentence(5)
#
#  30.times do |n|
#    Comment.create!(user_id: user.id,
#                    table_id: table.id,
#                    overal_rating: 5,
#                    table_rating: 5,
#                    surrounding_rating: 6,
#                    availability_rating: 5,
#                    parent_id: nil,
#                    content: content)
#   end
