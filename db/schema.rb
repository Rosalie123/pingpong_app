# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170409181212) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.integer  "table_id"
    t.integer  "user_id"
    t.string   "title"
    t.text     "content"
    t.string   "parent_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "votes_count", default: 0
    t.index ["parent_id"], name: "index_comments_on_parent_id", using: :btree
    t.index ["table_id"], name: "index_comments_on_table_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.integer  "bid_id"
    t.integer  "match_id"
    t.string   "subject"
    t.boolean  "read_sender",   default: true
    t.boolean  "read_receiver", default: false
    t.boolean  "personal",      default: false
    t.string   "status"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["bid_id"], name: "index_conversations_on_bid_id", using: :btree
    t.index ["match_id"], name: "index_conversations_on_match_id", using: :btree
    t.index ["receiver_id"], name: "index_conversations_on_receiver_id", using: :btree
    t.index ["sender_id"], name: "index_conversations_on_sender_id", using: :btree
  end

  create_table "liked_tables", force: :cascade do |t|
    t.integer  "table_id"
    t.integer  "liker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["liker_id"], name: "index_liked_tables_on_liker_id", using: :btree
    t.index ["table_id"], name: "index_liked_tables_on_table_id", using: :btree
  end

  create_table "match_bids", force: :cascade do |t|
    t.integer  "match_id"
    t.integer  "bidder_id"
    t.boolean  "accepted",   default: false
    t.boolean  "valid_bid",  default: true
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["bidder_id", "accepted"], name: "index_match_bids_on_bidder_id_and_accepted", using: :btree
    t.index ["bidder_id"], name: "index_match_bids_on_bidder_id", using: :btree
    t.index ["match_id"], name: "index_match_bids_on_match_id", using: :btree
    t.index ["valid_bid"], name: "index_match_bids_on_valid_bid", using: :btree
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "proposer_id"
    t.integer  "accepter_id"
    t.integer  "table_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.boolean  "confirmed",   default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["accepter_id", "proposer_id"], name: "index_matches_on_accepter_id_and_proposer_id", using: :btree
    t.index ["accepter_id"], name: "index_matches_on_accepter_id", using: :btree
    t.index ["proposer_id"], name: "index_matches_on_proposer_id", using: :btree
    t.index ["start_at", "end_at", "proposer_id"], name: "index_matches_on_start_at_and_end_at_and_proposer_id", using: :btree
    t.index ["start_at", "end_at", "table_id"], name: "index_matches_on_start_at_and_end_at_and_table_id", using: :btree
    t.index ["start_at", "end_at"], name: "index_matches_on_start_at_and_end_at", using: :btree
    t.index ["table_id"], name: "index_matches_on_table_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "author_id"
    t.integer  "conversation_id"
    t.boolean  "notice",          default: false
    t.text     "content"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["author_id"], name: "index_messages_on_author_id", using: :btree
    t.index ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "notifier_id"
    t.integer  "receiver_id"
    t.integer  "conversation_id"
    t.integer  "bid_id"
    t.string   "content"
    t.string   "title"
    t.boolean  "read",            default: false
    t.boolean  "seen",            default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["bid_id"], name: "index_notifications_on_bid_id", using: :btree
    t.index ["conversation_id"], name: "index_notifications_on_conversation_id", using: :btree
    t.index ["notifier_id"], name: "index_notifications_on_notifier_id", using: :btree
    t.index ["receiver_id"], name: "index_notifications_on_receiver_id", using: :btree
  end

  create_table "pictures", force: :cascade do |t|
    t.integer  "table_id"
    t.string   "picture"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["table_id"], name: "index_pictures_on_table_id", using: :btree
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "table_id"
    t.integer  "user_id"
    t.float    "surrounding_rating"
    t.float    "availability_rating"
    t.float    "table_rating"
    t.float    "ground_rating"
    t.float    "overal_rating"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["table_id"], name: "index_ratings_on_table_id", using: :btree
    t.index ["user_id"], name: "index_ratings_on_user_id", using: :btree
  end

  create_table "tables", force: :cascade do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "user_id"
    t.float    "table_rating"
    t.float    "surrounding_rating"
    t.float    "availability_rating"
    t.float    "overal_rating"
    t.float    "ground_rating"
    t.string   "table_number"
    t.boolean  "parking"
    t.boolean  "toilet"
    t.boolean  "picnic"
    t.boolean  "playground"
    t.string   "address"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["availability_rating"], name: "index_tables_on_availability_rating", using: :btree
    t.index ["ground_rating"], name: "index_tables_on_ground_rating", using: :btree
    t.index ["latitude", "longitude"], name: "index_tables_on_latitude_and_longitude", using: :btree
    t.index ["overal_rating"], name: "index_tables_on_overal_rating", using: :btree
    t.index ["surrounding_rating"], name: "index_tables_on_surrounding_rating", using: :btree
    t.index ["table_rating"], name: "index_tables_on_table_rating", using: :btree
    t.index ["user_id", "created_at"], name: "index_tables_on_user_id_and_created_at", using: :btree
    t.index ["user_id"], name: "index_tables_on_user_id", using: :btree
  end

  create_table "tables_users", id: false, force: :cascade do |t|
    t.integer "table_id"
    t.integer "user_id"
    t.index ["table_id"], name: "index_tables_users_on_table_id", using: :btree
    t.index ["user_id"], name: "index_tables_users_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "location"
    t.text     "description"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "comment_id"
    t.integer  "voter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_votes_on_comment_id", using: :btree
    t.index ["voter_id"], name: "index_votes_on_voter_id", using: :btree
  end

  add_foreign_key "comments", "tables"
  add_foreign_key "comments", "users"
  add_foreign_key "match_bids", "matches"
  add_foreign_key "matches", "tables"
  add_foreign_key "pictures", "tables"
  add_foreign_key "ratings", "tables"
  add_foreign_key "ratings", "users"
  add_foreign_key "tables", "users"
end
