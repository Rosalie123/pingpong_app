Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  root 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/login',  to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/signup', to: 'users#new'
  get '/inbox', to: 'conversations#index'
  patch '/notifications', to: 'notifications#seen'
  get '/dashboard', to: 'users#show'
  post '/conversations/new', to: 'conversations#new'






  resources :users do
    resources :matches, only: [:index, :edit, :update, :destroy]
    resources :match_bids, only: [:index, :edit, :update, :destroy]
    resources :conversations
  end

  resources :user_activations, only: [:edit]
  resources :sessions, only: [:create, :destroy]
  resources :tables do
    resources :ratings
    resources :matches
  end

  resources :matches do
    resources :match_bids
  end


  resources :matches
  resources :match_bids
  resources :comments do
    resources :votes
    resources :replies
  end

  resources :pictures

  resources :conversations do
    resources :messages
  end

  resources :messages, only: [:create]
  resources :notifications, only: [:update]
  resources :liked_tables, only: [:create, :destroy]






  resources :password_resets
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
